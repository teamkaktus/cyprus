<?php
/**
 * Основные параметры WordPress.
 *
 * Этот файл содержит следующие параметры: настройки MySQL, префикс таблиц,
 * секретные ключи, язык WordPress и ABSPATH. Дополнительную информацию можно найти
 * на странице {@link http://codex.wordpress.org/Editing_wp-config.php Editing
 * wp-config.php} Кодекса. Настройки MySQL можно узнать у хостинг-провайдера.
 *
 * Этот файл используется сценарием создания wp-config.php в процессе установки.
 * Необязательно использовать веб-интерфейс, можно скопировать этот файл
 * с именем "wp-config.php" и заполнить значения.
 *
 * @package WordPress
 */

// ** Параметры MySQL: Эту информацию можно получить у вашего хостинг-провайдера ** //
/** Имя базы данных для WordPress */
define('DB_NAME', 'cyprus');

/** Имя пользователя MySQL */
define('DB_USER', 'root');

/** Пароль к базе данных MySQL */
define('DB_PASSWORD', '');

/** Имя сервера MySQL */
define('DB_HOST', 'localhost');

/** Кодировка базы данных для создания таблиц. */
define('DB_CHARSET', 'utf8');

/** Схема сопоставления. Не меняйте, если не уверены. */
define('DB_COLLATE', '');

/**#@+
 * Уникальные ключи и соли для аутентификации.
 *
 * Смените значение каждой константы на уникальную фразу.
 * Можно сгенерировать их с помощью {@link https://api.wordpress.org/secret-key/1.1/salt/ сервиса ключей на WordPress.org}
 * Можно изменить их, чтобы сделать существующие файлы cookies недействительными. Пользователям потребуется снова авторизоваться.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '_HAXXLK+zxU_k,{r@F:s-O-3s6)~V.x__0I|iOH679}PD+@TqE)|)ClkY2aC^*!X');
define('SECURE_AUTH_KEY',  'J$| Q&-fH6((u(Li43GNb*=`9{h+Fvp?#(AL1]U*Pn8 F.X!C9&u-og!>]gRn_1N');
define('LOGGED_IN_KEY',    'AcRemkd;+Vme:X u;|z/# S[&W^eu{lS@B#G+oD--LBZ>Fz,uMrspy{`-_Mn.Pk`');
define('NONCE_KEY',        '?qWZ-UU%*ASY>(xc-n?XxY;kQP{/UD/h6S}_|ep_|ITW-$K<==Y<t-+r`mQ2+:|G');
define('AUTH_SALT',        'U+,p]s)F}v)@qHR}XVAz? mADDX=YYxBW//I|-N^5wu=KQAQ_/ze.R|#Yal1ILP1');
define('SECURE_AUTH_SALT', 'OlgG*WUpWX<f7`<}+*DHk]xXT_qLP*`t$19GRLmS(+>pK7-Q;dnn^7YXpfZww]tJ');
define('LOGGED_IN_SALT',   ';c([Wei0^5T-+5{Pg5VSPfYN^!UUjjZExkwk5Y>kd_ 7G8x{t3).y+Hu+GLgP,zD');
define('NONCE_SALT',       'm+B(32mdu*TUL_it|GV#4ZBo<>HBCJAM4(icl,y|RBa-{`ru]x+@s#=^d.a@BtOU');

/**#@-*/

/**
 * Префикс таблиц в базе данных WordPress.
 *
 * Можно установить несколько блогов в одну базу данных, если вы будете использовать
 * разные префиксы. Пожалуйста, указывайте только цифры, буквы и знак подчеркивания.
 */
$table_prefix  = 'wp_';

/**
 * Для разработчиков: Режим отладки WordPress.
 *
 * Измените это значение на true, чтобы включить отображение уведомлений при разработке.
 * Настоятельно рекомендуется, чтобы разработчики плагинов и тем использовали WP_DEBUG
 * в своём рабочем окружении.
 */
define('WP_DEBUG', false);

/* Это всё, дальше не редактируем. Успехов! */

/** Абсолютный путь к директории WordPress. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Инициализирует переменные WordPress и подключает файлы. */
require_once(ABSPATH . 'wp-settings.php');
