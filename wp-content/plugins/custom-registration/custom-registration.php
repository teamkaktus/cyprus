<?php

/*
  Plugin Name: Custom Registration
  Plugin URI: http://code.tutsplus.com
  Description: Updates user rating based on number of posts.
  Version: 1.0
  Author: Agbonghama Collins
  Author URI: http://tech4sky.com
 */
define('SHORTINIT', true);
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
global $wpdb;

function cyprus_email_exists($email){
    global $wpdb;
    $query = "SELECT id FROM ".$wpdb->prefix."cyprus_agent WHERE email = '".$email."'";
    //var_dump($query);
    return $wpdb->query($query);
}
function cyprus_phone_exists($phone){
    global $wpdb;
    $query = "SELECT id FROM ".$wpdb->prefix."cyprus_agent WHERE phone = '".$phone."'";
    //var_dump($query);
    return $wpdb->query($query);
}

function generate_password($number)
{
    $arr = array('a','b','c','d','e','f',
        'g','h','i','j','k','l',
        'm','n','o','p','r','s',
        't','u','v','x','y','z',
        'A','B','C','D','E','F',
        'G','H','I','J','K','L',
        'M','N','O','P','R','S',
        'T','U','V','X','Y','Z',
        '1','2','3','4','5','6',
        '7','8','9','0');

    /*
        ,'.',',',
        '(',')','[',']','!','?',
        '&','^','%','@','*','$',
        '<','>','/','|','+','-',
        '{','}','`','~'
    */
    // Генерируем пароль
    $pass = "";
    for($i = 0; $i < $number; $i++)
    {
        // Вычисляем случайный индекс массива
        $index = rand(0, count($arr) - 1);
        $pass .= $arr[$index];
    }
    return $pass;
}

function custom_registration_function() {
    if (isset($_POST['submit'])) {
        registration_validation(
            $_POST['surname'],
            $_POST['user_name'],
            $_POST['middlename'],
            $_POST['phone'],
            $_POST['email']
        );

        // sanitize user form input
        global $surname, $name, $middlename, $phone, $email;
        $surname    = 	sanitize_text_field($_POST['surname']);
        $name    	= 	sanitize_text_field($_POST['user_name']);
        $middlename = 	sanitize_text_field($_POST['middlename']);
        $email 		= 	sanitize_email($_POST['email']);
        $phone      = 	sanitize_text_field($_POST['phone']);

        // call @function complete_registration to create the user
        // only when no WP_error is found
        complete_registration(
            $surname,
            $name,
            $middlename,
            $phone,
            $email
        );
    }

    registration_form(
        $surname,
        $name,
        $middlename,
        $phone,
        $email
    );
}

function registration_form($surname, $name, $middlename, $phone, $email ) {

    $regform = "<form action='" . $_SERVER['REQUEST_URI'] . "' method=\"post\">
    <table class=\"form_container\">

                    <tr>
                        <td>
                            <input type=\"text\" placeholder=\"Фамилия\" name=\"surname\"
                            value=\"" . (isset($_POST['surname']) ? $surname : null) . "\">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type=\"text\" placeholder=\"Имя\" name=\"user_name\"
                            value=\"" . (isset($_POST['user_name']) ? $name : null) . "\">
                        </td>
                    </tr>
                     <tr>
                        <td>
                            <input type=\"text\" placeholder=\"Отчество\" name=\"middlename\"
                            value=\"" . (isset($_POST['middlename']) ? $middlename : null) . "\">
                        </td>
                    </tr>
                      <tr>
                        <td>
                            <input type=\"text\" placeholder=\"Моб. Телефон\" name=\"phone\"
                            value=\"" . (isset($_POST['phone']) ? $phone : null) . "\">
                        </td>
                    </tr>
                      <tr>
                        <td>
                            <input type=\"text\" placeholder=\"E-mail\" name=\"email\"
                            value=\"" . (isset($_POST['email']) ? $email : null) . "\">
                        </td>
                    </tr>
                    <tr>
                        <td>
                            <input type=\"submit\" name=\"submit\" value=\"Зарегистрироваться\">
                        </td>
                    </tr>

                </table></form>";
    echo $regform;
}

function registration_validation( $surname, $name, $middlename, $phone, $email )  {
    global $reg_errors;
    $reg_errors = new WP_Error;

    if ( empty( $surname ) || empty( $name ) || empty( $middlename )|| empty( $phone )|| empty( $email ) ) {
        $reg_errors->add('field', 'Не все поля заполнены');
    }


    if (!preg_match("/[^a-z,A-Z,0-9,а-яіїєґ,А-ЯІЇЄҐ,-,_]/", strtolower($surname))) {
        $reg_errors->add('surname_invalid', 'Фамилия не корректна');
    }
    if (!preg_match("/[^a-z,A-Z,0-9,а-яіїєґ,А-ЯІЇЄҐ,-,_]/", strtolower($name))) {
        $reg_errors->add('name_invalid', 'Имя не корректно');
    }
    if (!preg_match("/[^a-z,A-Z,0-9,а-яіїєґ,А-ЯІЇЄҐ,-,_]/", strtolower($middlename))) {
        $reg_errors->add('middlename_invalid', 'Отчество не корректно');
    }

    if (!preg_match("/^((8|\+7)[\- ]?)?(\(?\d{3}\)?[\- ]?)?[\d\- ]{7,10}$/", strtolower($phone))) {
        $reg_errors->add('phone_invalid', 'Телефон не корректен');
    }
    if(cyprus_phone_exists($phone)){
        $reg_errors->add('phone', 'Телефон уже используется');
    }


    if ( !is_email( $email ) ) {
        $reg_errors->add('email_invalid', 'Почта не коректна');
    }

    if ( cyprus_email_exists( $email ) ) {
        $reg_errors->add('email', 'Почтовый ящик уже используется');
    }


    if ( is_wp_error( $reg_errors ) ) {

        foreach ( $reg_errors->get_error_messages() as $error ) {
            echo '<div>';
            echo '<strong>ERROR</strong>:';
            echo $error . '<br/>';

            echo '</div>';
        }
    }
}

function complete_registration() {
    global $reg_errors, $wpdb, $surname, $name, $middlename, $phone, $email ;

    if ( count($reg_errors->get_error_messages()) < 1 ) {
       /* $userdata = array(
            'surname'	 => 	$surname,
            'user_name'  => 	$name,
            'middlename' => 	$middlename,
            'phone' 	 => 	$phone,
            'email' 	 => 	$email,

        );*/
        //$user = wp_insert_user( $userdata );echo("УДАЧНЫЙ РЕГ");
        $regdate  = "".date('d.m.Y');
        $type     = "2";

        $password = "".generate_password(8);

        $query = "INSERT INTO ".$wpdb->prefix."cyprus_agent
        VALUES ('', '".$surname."',
         '".$name."',
         '".$middlename."',
         '".$phone."',
         '".$email."',
         '".$password."',
         '".$regdate."',
         '".$type."'
         )";
        //var_dump($query);
        if($wpdb->query($query)){
            echo 'Регистрация завершена';


            /* получатели */
            $to= "Agent <".$email.">" ;

            /* тема/subject */
            $subject = "Успешная регистрация";

            /* сообщение */
            $message = '
            <html>
            <head>
             <title>Данные авторизации</title>
            </head>
            <body>
            <p>Авторизация прошла успешно!</p>
            <p>Логин</p>
            <p>'.$email.'</p>
            <p>Пароль</p>
            <p>'.$password.'</p>
            </body>
            </html>
            ';

            /* Для отправки HTML-почты вы можете установить шапку Content-type. */
            $headers= "MIME-Version: 1.0\r\n";
            $headers .= "Content-type: text/html; charset=utf-8\r\n";

            /* дополнительные шапки */
            /*$headers .= "From: Birthday Reminder <birthday@example.com>\r\n";
            $headers .= "Cc: birthdayarchive@example.com\r\n";
            $headers .= "Bcc: birthdaycheck@example.com\r\n";*/

            /* и теперь отправим из */
            mail($to, $subject, $message, $headers);

        }else{
            echo 'Ошибка регистрация';
        }

    }
}

// Register a new shortcode: [cr_custom_registration]
add_shortcode('cr_custom_registration', 'custom_registration_shortcode');

// The callback function that will replace [book]
function custom_registration_shortcode() {
    ob_start();
    custom_registration_function();
    return ob_get_clean();
}
