<?php
global $wpdb;

if (function_exists('wp_enqueue_media')) {
    wp_enqueue_media();
}

?>
<style>
    table img {
        width: 64px;
        height: 64px;
    }

</style>

<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>

<div id="add_news_container">
    <form id="add_news_f" class="form-horizontal" enctype="multipart/form-data">

        <table class="table">
            <tr>

                <th>Название</th>
                <th><input type="button" class="btn btn-default" id="news_img_b" name="news_img"
                           value="Задать фото"></th>
            </tr>
            <tr>
                <td><input type="text" class="form-control" id="news_title" name="news_title"></td>

                <td><input type="text" id="news_img_url" name="news_img_url" hidden><img id="news_img" src=" ">
                </td>

                <td><input type="button" class="btn btn-default" id="add_news" value="Добавить"></td>
            </tr>
        </table>
        <textarea id="news_content" class='news_content' name="news_content" type="text"></textarea>
    </form>

</div>
<div id="news_container">
    <h3>Существующие категории</h3>
    <table class="table">
        <tr>
            <th>Дата</th>
            <th>Название</th>
            <th>Изображение</th>
            <th class="text-center" colspan="2">Удалить/Изменить</th>
        </tr>
        <?php
        $cat = $wpdb->get_results("SELECT *  FROM " . $wpdb->prefix . "cyprus_news AS news
        ORDER BY news.`date` DESC", OBJECT_K);

        //var_dump($cat);
        foreach ($cat as $row) {

            $view_news .= "<tr>
            <td><input type='text' class=\"form-control\" name='upd_news_date" . $row->id . "' value='" . $row->date . "' ></td>";
            $view_news .= "<td><input type='text' class=\"form-control\" name='upd_news_title" . $row->id . "' value='" . $row->title . "' ></td>";
            $view_news .= " <td><img class='upd_news_img' id='img_upd_news_img" . $row->id . "' src=\"http://" . $_SERVER['SERVER_NAME'] . $row->image . "\">
                                  <input id='inp_upd_news_img" . $row->id . "' value='" . $row->image . "' hidden></td>";

            $view_news .= "<td><input style='width:100%;' type=\"button\" value=\"Удалить\" class=\"del_news btn btn-default\" id=\"del_news" . $row->id . "\">";

            $view_news .= "<td><input style='width:100%;' type='button' value='Обновить' class='upd_news btn btn-default' id='upd_news" . $row->id . "'></td></tr>";
            $view_news .= "<tr><td colspan='7'><textarea class='news_content' id=\"news_content" . $row->id . "\" name=\"news_content\" type=\"text\">" . $row->content . "</textarea></td></tr>";

        }

        echo $view_news;
        ?>

    </table>
</div>
