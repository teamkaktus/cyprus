<?php
/*
Plugin Name: custom_settings
Plugin URI: http://example.com/
Description: управление дополнительными функциями сайта
Version: 1.0
Author: example
Author URI: http://example.com
*/
global $wpdb;

define('DIV__PLUGIN_URL', plugin_dir_url(__FILE__));
define('DIV__PLUGIN_DIR', plugin_dir_path(__FILE__));

//require_once(DIV__PLUGIN_DIR . 'functions.php');

add_action('admin_init', 'admin_init_style');
add_action('admin_menu', 'add_pages');

function add_pages()
{
    /*
     * Страница Главная
     */
    $menu_item = add_menu_page('Дополнительные опции', 'Дополнительные опции', 'manage_options', 'custom_settings', 'plugin_page_main', 'dashicons-admin-settings', 2000);
    add_action('admin_print_scripts-' . $menu_item, 'plugin_main_add_items_scripts');
    /*
     * Страница Агентов
     */
    $menu_item2 = add_submenu_page('custom_settings', 'Агенты', 'Агенты', 1, 'agents', 'plugin_page_agents');
    add_action('admin_print_scripts-' . $menu_item2, 'plugin_agents_add_items_scripts');
    /*
     * Страница Новостей
     */
    $menu_item2 = add_submenu_page('custom_settings', 'Новости', 'Новости', 1, 'news', 'plugin_page_news');
    add_action('admin_print_scripts-' . $menu_item2, 'plugin_news_add_items_scripts');

}

function admin_init_style()
{
    wp_enqueue_style('style');


    wp_register_script('jquery.js', DIV__PLUGIN_URL . '/js/jquery-2.1.3.min.js', $ver = false, $in_footer = false);
    wp_register_script('jquery-ui.min.js', DIV__PLUGIN_URL . '/js/jquery-ui.min.js', $ver = false, $in_footer = false);
    wp_register_script('tinyMCE.js',       "//cdn.tinymce.com/4/tinymce.min.js", $ver = false, $in_footer = false);


    wp_register_script('news.js', DIV__PLUGIN_URL . '/js/news.js', $ver = false, $in_footer = false);
    wp_register_script('agents.js', DIV__PLUGIN_URL . '/js/agents.js', $ver = false, $in_footer = false);


    wp_localize_script('news.js', 'news_ajax', array('url' => DIV__PLUGIN_URL . 'scripts/news.php'));
    wp_localize_script('agents.js', 'agent_ajax', array('url' => DIV__PLUGIN_URL . 'scripts/agents.php'));
}

/*
 * Страница Главная
 */
function plugin_main_add_items_scripts()
{
    wp_enqueue_script('jquery.js');
    wp_enqueue_script('js_main.js');
    wp_enqueue_style('thickbox');
    wp_enqueue_script('media-upload');

}

function plugin_page_main()
{
    load_template(DIV__PLUGIN_DIR . 'view_main.php', true);
}

/*
 * Страница Агенты
 */
function plugin_agents_add_items_scripts()
{
    wp_enqueue_script('jquery.js');
    wp_enqueue_script('agents.js');
    wp_enqueue_style('view_agent.css',  DIV__PLUGIN_URL . '/css/view_agent.css');
}

function plugin_page_agents()
{
    load_template(DIV__PLUGIN_DIR . 'view_agents.php', true);
}

/*
 * Страница Новости
 */
function plugin_news_add_items_scripts()
{
    wp_enqueue_script('jquery.js');
    wp_enqueue_script( 'tinyMCE.js' );
    wp_enqueue_script('news.js');

}

function plugin_page_news()
{
    load_template(DIV__PLUGIN_DIR . 'view_news.php', true);
}
