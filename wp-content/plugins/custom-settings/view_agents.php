<?php
global $wpdb;
?>
<head>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/css/bootstrap-theme.min.css">
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.4/js/bootstrap.min.js"></script>
</head>
<!-- AGENT END-->
<div class="row agent">

        <div class="row">
            <div class="container">
                <h1>Агенты</h1>
            </div>
        </div>
        <div class="row">

            <?php
            $query = "SELECT * FROM " . $wpdb->prefix . "cyprus_agent";
            $res = $wpdb->get_results($query);

            ?>
            <select name="" id="agents_list">
                <option value="0">- Выберите агента -</option>
                <?php
                foreach ($res as $agent) {


                    ?>
                    <option value="<?php echo $agent->id; ?>">
                        <?php
                        echo($agent->f . " " . $agent->i . " " . $agent->o . " ");
                        ?>
                    </option>
                    <?php
                }
                ?>
            </select>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table id="agent_data" style="display: ;">
                    <tr>
                        <th>ФИО</th>
                        <th>Телефон</th>
                        <th>E-mail</th>
                        <th>Дата регистрации</th>
                        <th>Вид деятельности</th>
                        <th>Кол-во клиентов</th>
                    </tr>
                    <tr>
                        <td id="fio"></td>
                        <td id="phone"></td>
                        <td id="email"></td>
                        <td id="regdate"></td>
                        <td id="type"></td>
                        <td id="clients_count"></td>
                    </tr>
                </table>
            </div>
            <div class="col-md-12" id="passports">

            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <table id="clients" style="display: none;">
                    <tr>
                        <th>№п.п.</th>
                        <th>ФИО клиента</th>
                        <th>Телефон</th>
                        <th>E-mail</th>
                        <th>Дата регистр.</th>
                        <th>Время регистр.</th>
                        <th>Тип клиента</th>
                        <th>Статус клиента</th>
                        <th>ФИО агента</th>
                        <th>Телефон агента</th>
                        <th>Дата беседы</th>
                        <th>Время беседы</th>
                    </tr>

                </table>
            </div>
        </div>

</div>
<!-- AGENT END-->