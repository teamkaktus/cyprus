/*
 из сериализованого объекта формы делает ассоциативный массив
 */
function serializedFormToArray(data){
    var index = 0;
    var res = {};
    res[data[index]['name']] = data[index]['value'];
    for (index = 1; index < data.length; ++index) {
        res[data[index]['name']] = data[index]['value'];
    }

    return res;
}

$(document).ready(function() {

    function upd_client(event) {
        var a = new Object;
        a.id = event.target.id;
        a.status = $("#status_"+a.id).val();

        var d = JSON.stringify(a);
        $.ajax({
                url: agent_ajax.url,
                type: 'POST',
                data: {
                    event: 'upd_client',
                    data: d
                },
            })
            .done(function (res) {

                console.log(res);
            });
    }

    $("#agents_list").change(function(event) {

        b = $("#agents_list").val();
        $.ajax({
                url: agent_ajax.url,
                type: 'POST',
                data: {
                    event : 'get_agent',
                    data  : b
                },
            })
            .done(function(data) {

                var res = JSON.parse(data);
                $("#fio").html(res.f+" "+res.i+" "+res.o);
                $("#phone").html(res.phone);
                $("#email").html(res.email);
                $("#regdate").html(res.regdate);
                $("#type").html(res.type);
                $("#clients_count").html(res.clients_count);

                $("#agent_data").css("display","table");

                var urls = res.passports;
                $("#passports").html("");
                urls.forEach(function(entry){
                    var img = "<img src='"+entry.url+"'>";
                    $("#passports").append(img);
                });

                $.ajax({
                        url: agent_ajax.url,
                        type: 'POST',
                        data: {
                            event : 'get_cliets_list',
                            data  : res.id
                        },
                    })
                    .done(function(data){
                        if(data != ""){
                            var res = JSON.parse(data);

                            $("#clients").html("<tr>"+
                                "<th>ФИО клиента</th>"+
                                "<th>Телефон</th>"+
                                "<th>E-mail</th>"+
                                "<th>Дата регистр.</th>"+
                                "<th>Время регистр.</th>"+
                                "<th>Тип клиента</th>"+
                                "<th>Статус клиента</th>"+
                                "<th>Дата беседы</th>"+
                                "<th>Время беседы</th>"+
                                "</tr>"
                            );
                            res.forEach(function(entry){
                                $("#clients").append("<tr>" +
                                    "<td>"+entry.fio+"</td>" +
                                    "<td>"+entry.phone+"</td>" +
                                    "<td>"+entry.email+"</td>" +
                                    "<td>"+entry.reg_date+"</td>" +
                                    "<td>"+entry.reg_time+"</td>" +
                                    "<td>"+entry.type+"</td>" +
                                    "<td><input type='text' id='status_"+entry.id+"' value='"+entry.status+"'>"+
                                    "<span id='"+entry.id+
                                    "' class=\"upd_client glyphicon glyphicon-refresh\" aria-hidden=\"true\"></span>" +
                                    "</td>" +
                                    "<td>"+entry.conv_date+"</td>" +
                                    "<td>"+entry.conv_time+"</td>" +
                                    "</tr>"
                                );

                            });
                            $(".upd_client").click(upd_client);
                        }else{
                            $("#clients").html("");
                        }

                        $("#clients").css("display","table");
                    });

               // alert(res);
               // setTimeout(function() {window.location.reload();}, 1000);
            });

    });





});


