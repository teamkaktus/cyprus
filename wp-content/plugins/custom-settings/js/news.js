/*
 из сериализованого объекта формы делает ассоциативный массив
 */
function serializedFormToArray(data){
    var index = 0;
    var res = {};
    res[data[index]['name']] = data[index]['value'];
    for (index = 1; index < data.length; ++index) {
        res[data[index]['name']] = data[index]['value'];
    }

    return res;
}

$(document).ready(function() {

    tinymce.init({ selector:'textarea.news_content' });

    $('#news_img_b').click(function(e) {
        var custom_logo_uploader;
        e.preventDefault();
        //If the uploader object has already been created, reopen the dialog
        if (custom_logo_uploader) {
            custom_logo_uploader.open();
            return;
        }
        //Extend the wp.media object
        custom_logo_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Выберите изображения',
            button: {
                text: 'Выбрать'
            },
            multiple: false
        });
        //When a file is selected, grab the URL and set it as the text field's value
        custom_logo_uploader.on('select', function() {
            attachment = custom_logo_uploader.state().get('selection').first().toJSON();
            $('#news_img').attr('width','32');
            $('#news_img').attr('height','32');
            $('#news_img').attr('src',attachment.url);
            //$('#ftrimg').attr('src',attachment.sizes.thumbnail.url);
            var url = document.createElement('a');
            url.href = attachment.url;

            $('#news_img_url').val(url.pathname);

        });
        //Open the uploader dialog
        custom_logo_uploader.open();
    });

    $("#add_news").click(function(event) {
        var a = $("#add_news_f").serializeArray();
        var c = serializedFormToArray(a);
        c.news_content = tinyMCE.get('news_content').getBody().innerHTML;
        var b = JSON.stringify(c);
        $.ajax({
                url: news_ajax.url,
                type: 'POST',
                data: {
                    event : 'add_news',
                    data  : b
                },
            })
            .done(function(data) {
                alert(data);
                setTimeout(function() {window.location.reload();}, 1000);
            });

    });

    $(".del_news").click(function(event) {
        var a = event.target.id;
        a = a.replace("del_news", '');
        $.ajax({
                url: news_ajax.url,
                type: 'POST',
                data: {
                    event : 'del_news',
                    data  : a
                },
            })
            .done(function(data) {
                alert(data);
                setTimeout(function() {window.location.reload();}, 1000);
            });
    });

    $(".upd_news_img").click(function(e){
        var a = event.target.id;
        a = a.replace("img_upd_news_img", '');

        var custom_logo_uploader;
        e.preventDefault();
        //If the uploader object has already been created, reopen the dialog
        if (custom_logo_uploader) {
            custom_logo_uploader.open();
            return;
        }
        //Extend the wp.media object
        custom_logo_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Выберите изображения',
            button: {
                text: 'Выбрать'
            },
            multiple: false
        });
        //When a file is selected, grab the URL and set it as the text field's value
        custom_logo_uploader.on('select', function() {
            attachment = custom_logo_uploader.state().get('selection').first().toJSON();

            $("#img_upd_news_img"+a).attr('src',attachment.url);
            var url = document.createElement('a');
            url.href = attachment.url;


            $("#inp_upd_news_img"+a).val(url.pathname);

            //tmp = $("s").val();


        });
        //Open the uploader dialog
        custom_logo_uploader.open();
    });

    $(".upd_news").click(function(event){
        var a = event.target.id;
        a = a.replace("upd_news", '');
        var d = new Object();
        d.id = a;
        d.news_date = $("input[name='upd_news_date"+a+"']").val();
        d.news_title = $("input[name='upd_news_title"+a+"']").val();

        var url = document.createElement('a');
        url.href = $("#img_upd_news_img"+a).attr('src');
        d.news_img_url = url.pathname;

        d.news_content = tinyMCE.get('news_content'+a).getBody().innerHTML;

        var res = JSON.stringify(d);

        $.ajax({
                url: news_ajax.url,
                type: 'POST',
                data: {
                    event : 'upd_news',
                    data  : res
                },
            })
            .done(function(data) {
                alert(data);
               // setTimeout(function() {window.location.reload();}, 100);
            });

    });



});


