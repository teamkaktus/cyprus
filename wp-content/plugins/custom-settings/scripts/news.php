<?php
define('SHORTINIT', true);
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
global $wpdb;

function get_all_news($db)
{
    $query = "SELECT * FROM " . $db->prefix . "cyprus_news AS news ORDER BY news.`date` ASC";
    $items = $db->get_results($query);
    if (isset($items[0])) {
        return $items;
    }
    return NULL;
}

function add_news($data, $db)
{
    $tmp = json_decode($data);
    if (isset($tmp->news_title)) {
        if ($tmp->news_title != "") {
            $date = "" . date("Y-m-d H:i:s");
            $query = "INSERT INTO " . $db->prefix . "cyprus_news VALUES ('',
             '" . $date . "',
              '" . $tmp->news_title . "',
               '" . $tmp->news_content . "',
                '" . $tmp->news_img_url . "')";
            $db->query($query);
            return "Новость создана";
        } else {
            return "Отправлена пустая строка";
        }
    }
    return "Отправлены неверные данные";
}

function upd_news($data, $db)
{

    if (isset($data)) {
        $news = json_decode($data);
        $query = "UPDATE " . $db->prefix . "cyprus_news AS news
                    SET
                    news.`date` = '" . $news->news_date . "',
                    news.title = '" . $news->news_title . "',
                    news.content = '" . $news->news_content . "',
                    news.image = '" . $news->news_img_url . "'
                    WHERE
                    news.id = " . $news->id;

        if ($db->query($query)) {
            return "Новость обновлена";
        } else {
            return "Новость обновлена";
        }
    }
    return "Отправлены неверные данные";

}

function del_news($data, $db)
{
    if (isset($data)) {

        $query = "DELETE FROM " . $db->prefix . "cyprus_news WHERE id = " . $data;
        $db->query($query);
        return "Новость удалена";
    }
    return "Отправлены неверные данные";
}

if (isset($_POST['event'])) {
    switch ($_POST['event']) {
        case 'add_news':
            $result = add_news($_POST['data'], $wpdb);
            break;
        case 'del_news':
            $result = del_news($_POST['data'], $wpdb);
            break;
        case 'upd_news':
            $result = upd_news($_POST['data'], $wpdb);
            break;
        case 'get_news':
            $result = get_news();
            break;
    }
}
echo $result;
?>