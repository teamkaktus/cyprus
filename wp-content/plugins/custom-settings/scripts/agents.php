<?php
define('SHORTINIT', true);
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
global $wpdb;


function get_agent($data, $db)
{
    $id = "" . $data;
    $query = "SELECT agents.id,
                agents.f,
                agents.i,
                agents.o,
                agents.phone,
                agents.email,
                agents.regdate,
                agents.type
              FROM " . $db->prefix . "cyprus_agent AS agents WHERE agents.id = " . $id . "";

    $res = $db->get_results($query);

    if (isset($res[0])) {
        $res = $res[0];
        $query = "SELECT
                    wp_cyprus_agent_types.`desc` AS type
                  FROM
                     " . $db->prefix . "cyprus_agent_types
                  WHERE
                    wp_cyprus_agent_types.id IN
                  (SELECT type FROM " . $db->prefix . "cyprus_agent WHERE id = '" . $res->id . "')";

        $agent_type = $db->get_results($query);
        $res->type = $agent_type[0]->type;

        $query = "SELECT Count(id) AS c_count
                    FROM " . $db->prefix . "cyprus_clients AS clients
                    WHERE clients.agent_id IN
                    (SELECT id FROM " . $db->prefix . "cyprus_agent WHERE id = '" . $res->id . "')";

        $clients_count = $db->get_results($query);
        $res->clients_count = $clients_count[0]->c_count;

        $query = "SELECT url FROM " . $db->prefix . "cyprus_agent_passports WHERE agent_id = '" . $res->id . "'";
        $res_url = $db->get_results($query);
        $i=0;
        foreach($res_url as $entry){
            $res->passports[$i] = $entry->url;
            $i++;
        }
        /*var_dump($res->passports);
        exit;*/
        $res->passports = $res_url;
        return json_encode($res);
    }
    return NULL;

}

function get_cliets_list($data, $wpdb)
{
    $id = "" . $data;
    $query = "SELECT *
        FROM " . $wpdb->prefix . "cyprus_clients AS clients
        WHERE clients.agent_id IN (SELECT id FROM wp_cyprus_agent WHERE id = '" . $id . "')";

    $res = $wpdb->get_results($query);

    if (isset($res[0])) {
        return json_encode($res);
    }
    return NULL;
}

function upd_client($data, $wpdb)
{
    $d = json_decode($data);
    $query = "UPDATE " . $wpdb->prefix . "cyprus_clients
                SET status = '" . $d->status . "'
                WHERE id = '" . $d->id . "'";
    if ($wpdb->query($query)) {
        return "Клиент обновлён";
    } else {
        return "Ошибка обновления";
    }
}

$result = "";
if (isset($_POST['event'])) {
    switch ($_POST['event']) {
        case 'get_agent':
            $result = get_agent($_POST['data'], $wpdb);
            break;
        case 'get_cliets_list':
            $result = get_cliets_list($_POST['data'], $wpdb);
            break;
        case 'upd_client':
            $result = upd_client($_POST['data'], $wpdb);
            break;
    }
}
echo $result;
?>