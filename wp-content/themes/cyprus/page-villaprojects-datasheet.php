<?php
/*
Template Name: Проекты вилл - Техническая спецификация
*/
?>
<?php

get_header();

?>
</div>
</div>
</div>
</div>
<!-- HEADER END-->

<!-- DATASHEET START-->
<div class="row datasheet">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <span class="masterplan-title">Техническая спецификация</span>
            </div>
        </div>
        <?php

        $my_post_obj = get_post(get_the_ID()); // параметр функции - ID поста, содержимое которого нужно вывести
        echo $my_post_obj->post_content;
        ?>

    </div>


</div>
<!-- DATASHEET END-->
<?php

get_footer();

?>
