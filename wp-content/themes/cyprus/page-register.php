<?php
/*
Template Name: Register
*/
?>
<?php

session_start(); //Запускаем сессии

class AuthClass
{
    /**
     * Проверяет, авторизован пользователь или нет
     * Возвращает true если авторизован, иначе false
     * @return boolean
     */
    public function isAuth()
    {
        if (isset($_SESSION["is_auth"])) { //Если сессия существует
            return $_SESSION["is_auth"]; //Возвращаем значение переменной сессии is_auth (хранит true если авторизован, false если не авторизован)
        } else return false; //Пользователь не авторизован, т.к. переменная is_auth не создана
    }

    /**
     * Авторизация пользователя
     * @param string $login
     * @param string $passwors
     */
    public function auth($login, $password)
    {
        global $wpdb;
        $query = "SELECT password, type FROM " . $wpdb->prefix . "cyprus_agent WHERE email = '" . $login . "'";

        $res = $wpdb->get_results($query);

        if ($res[0]->password == $password && $res[0]->type > 1) { //Если логин и пароль введены правильно
            $_SESSION["is_auth"] = true; //Делаем пользователя авторизованным
            $_SESSION["login"] = $login; //Записываем в сессию логин пользователя
            return true;
        } else { //Логин и пароль не подошел
            $_SESSION["is_auth"] = false;
            return false;
        }
    }

    /**
     * Метод возвращает логин авторизованного пользователя
     */
    public function getLogin()
    {
        if ($this->isAuth()) { //Если пользователь авторизован
            return $_SESSION["login"]; //Возвращаем логин, который записан в сессию
        }
    }


    public function out()
    {
        $_SESSION = array(); //Очищаем сессию
        session_destroy(); //Уничтожаем
    }
}

$auth = new AuthClass();

if ($_SESSION["is_auth"]) { // Если пользователь авторизован, приветствуем:
    header('Location: /agent', true, 303);
}

if (isset($_POST["auth"])) {
    if (isset($_POST["login"]) && isset($_POST["password"])) { //Если логин и пароль были отправлены
        if (!$auth->auth($_POST["login"], $_POST["password"])) { //Если логин и пароль введен не правильно
            $notification = "Логин и пароль введен не правильно!";
        }else{
            header('Location: /agent', true, 303);
        }
    }
}

if (isset($_POST["repass"])) {
    if (isset($_POST["login"])) {

        if (is_email($_POST["login"])) {
            $query = "SELECT id FROM " . $wpdb->prefix . "cyprus_agent WHERE email = '" . $_POST["login"] . "'";
            $res = $wpdb->get_results($query);

            if (isset($res[0]->id)) {
                $password = "" . generate_password(8);
                $query = "UPDATE " . $wpdb->prefix . "cyprus_agent AS agents
                 SET agents.password = '" . $password . "'
                 WHERE agents.id = '" . $res[0]->id . "'";

                if ($wpdb->query($query)) {
                    /* получатели */
                    $to = "Agent <" . $_POST["login"] . ">";

                    /* тема/subject */
                    $subject = "Восстановление пароля";

                    /* сообщение */
                    $message = '
                        <html>
                        <head>
                         <title>Данные авторизации</title>
                        </head>
                        <body>
                        <p>Пароль восстановлен!</p>
                        <p>Новые авторизационные  данные:</p>
                        <p>Логин</p>
                        <p>' . $_POST["login"] . '</p>
                        <p>Пароль</p>
                        <p>' . $password . '</p>
                        </body>
                        </html>
                        ';

                    /* Для отправки HTML-почты вы можете установить шапку Content-type. */
                    $headers = "MIME-Version: 1.0\r\n";
                    $headers .= "Content-type: text/html; charset=utf-8\r\n";

                    /* дополнительные шапки */
                    /*$headers .= "From: Birthday Reminder <birthday@example.com>\r\n";
                    $headers .= "Cc: birthdayarchive@example.com\r\n";
                    $headers .= "Bcc: birthdaycheck@example.com\r\n";*/

                    /* и теперь отправим из */
                    mail($to, $subject, $message, $headers);

                    $notification =  "Новый пароль отправлен Вам на Email";
                } else {
                    $notification =  "Ошибка восстановления. Обратитесь к Администратору.";
                }
            } else {
                $notification =  "Такой Email не существует";
            }

        } else {
            $notification =  "Введите правильный Email";
        }

    } else {
        $notification =  "Введите логин";
    }
}
get_header();

?>
    </div>
    </div>
    </div>
    </div>
    <!-- HEADER END-->
    <!-- REGISTER START-->
<?php
define('SHORTINIT', true);
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
global $wpdb;

$login = sanitize_text_field($_POST['login']);
$password = sanitize_text_field($_POST['password']);


/**
 * Класс для авторизации
 */

?>
    <div class="row register">
        <div class="container">
            <div class="col-md-4 col-md-offset-1 col-xs-12">
                <h1 class="text-center">Вход</h1>
                <?php
                echo $notification;



                ?>
                <table class="form_container">
                    <form action='<?php $_SERVER['REQUEST_URI'] ?>' method="post">
                        <tr>
                            <td>
                                <input type="text" placeholder="Логин/E-mail" name="login"
                                       value="<?php if (isset($_POST['login'])) {
                                           echo $_POST['login'];
                                       } ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="password" placeholder="Пароль" name="password"
                                       value="<?php if (isset($_POST['password'])) {
                                           echo $_POST['password'];
                                       } ?>">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <img style="float: left;"
                                     src="<?php echo esc_url(get_template_directory_uri()); ?>/images/small-key.png"
                                     alt="">
                                <input type="submit" id="repass" name="repass" value="Восстановить пароль">
                            </td>
                        </tr>
                        <tr>
                            <td>
                                <input type="submit" name="auth" value="Войти">
                            </td>
                        </tr>
                    </form>
                </table>
            </div>
            <div class="col-md-4 col-md-offset-1 col-xs-12">
                <h1 class="text-center">Регистрация</h1>
                <?php custom_registration_function(); ?>
            </div>
        </div>
    </div>
    <!-- REGISTER END-->

<?php

get_footer();

?>