<?php
/*
Template Name: Инфраструктура - Внешняя
*/
?>
<?php

get_header();

?>
</div>
</div>
</div>
</div>
<!-- HEADER END-->
<!-- INFRASTRUCTURE START -->
<div class="row infrastructure">

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <span class="infrastructure-title">Внешняя инфраструктура</span>
            </div>

        </div>
        <div class="row inf">
            <div class="inf-tabs col-sm-4 col-xs-12">

                <!-- Nav tabs -->
                <ul class="nav nav-tabs nav-stacked" role="tablist">
                    <li role="presentation" <?php
                        if($_GET["p"]=="school" || !isset($_GET["p"]) || $_GET["p"]==""){
                            echo("class=\"active\"");
                        }
                    ?>>
                        <a href="#school" aria-controls="school" role="tab" data-toggle="tab"><span></span>Английская школа</a>
                    </li>
                    <li role="presentation" <?php
                    if($_GET["p"]=="zoo"){
                        echo("class=\"active\"");
                    }
                    ?>>
                        <a href="#zoo" aria-controls="zoo" role="tab" data-toggle="tab"><span></span>Зоопарк</a>
                    </li>
                    <li role="presentation" <?php
                    if($_GET["p"]=="beach"){
                        echo("class=\"active\"");
                    }
                    ?>>
                        <a href="#beach" aria-controls="beach" role="tab" data-toggle="tab"><span></span>Пляж</a>
                    </li>
                    <li role="presentation" <?php
                    if($_GET["p"]=="supermarket"){
                        echo("class=\"active\"");
                    }
                    ?>>
                        <a href="#supermarket" aria-controls="supermarket" role="tab" data-toggle="tab"><span></span>Супермаркет</a>
                    </li>
                    <li role="presentation" <?php
                    if($_GET["p"]=="reserve"){
                        echo("class=\"active\"");
                    }
                    ?>>
                        <a href="#reserve" aria-controls="reserve" role="tab" data-toggle="tab"><span></span>Заповедник</a>
                    </li>
                    <li role="presentation" <?php
                    if($_GET["p"]=="ranch"){
                        echo("class=\"active\"");
                    }
                    ?>>
                        <a href="#ranch" aria-controls="ranch" role="tab" data-toggle="tab"><span></span>Ранчо</a>
                    </li>
                    <li role="presentation" <?php
                    if($_GET["p"]=="church"){
                        echo("class=\"active\"");
                    }
                    ?>>
                        <a href="#church" aria-controls="church" role="tab" data-toggle="tab"><span></span>Храм</a>
                    </li>
                    <li role="presentation" <?php
                    if($_GET["p"]=="pier"){
                        echo("class=\"active\"");
                    }
                    ?>>
                        <a href="#pier" aria-controls="pier" role="tab"
                           data-toggle="tab"><span></span>Причал для яхт</a>
                    </li>

                </ul>
            </div>
            <div class="inf-content col-sm-8 col-xs-12">
                <!-- Tab panes -->
                <div class="tab-content">
                    <div role="tabpanel" class="tab-pane <?php
                    if($_GET["p"]=="school" || !isset($_GET["p"]) || $_GET["p"]==""){
                        echo(" active");
                    }
                    ?>" id="school">

                        <?php

                        $my_post_obj = get_post(211); // параметр функции - ID поста, содержимое которого нужно вывести
                        echo $my_post_obj->post_content;
                        ?>

                    </div>
                    <div role="tabpanel" class="tab-pane <?php
                    if($_GET["p"]=="zoo"){
                        echo(" active");
                    }
                    ?>" id="zoo">

                        <?php

                        $my_post_obj = get_post(215); // параметр функции - ID поста, содержимое которого нужно вывести
                        echo $my_post_obj->post_content;
                        ?>

                    </div>
                    <div role="tabpanel" class="tab-pane <?php
                    if($_GET["p"]=="beach"){
                        echo(" active");
                    }
                    ?>" id="beach">
                        <?php

                        $my_post_obj = get_post(217); // параметр функции - ID поста, содержимое которого нужно вывести
                        echo $my_post_obj->post_content;
                        ?>

                    </div>
                    <div role="tabpanel" class="tab-pane <?php
                    if($_GET["p"]=="supermarket"){
                        echo(" active");
                    }
                    ?>" id="supermarket">
                        <?php

                        $my_post_obj = get_post(219); // параметр функции - ID поста, содержимое которого нужно вывести
                        echo $my_post_obj->post_content;
                        ?>

                    </div>
                    <div role="tabpanel" class="tab-pane <?php
                    if($_GET["p"]=="reserve"){
                        echo(" active");
                    }
                    ?>" id="reserve">
                        <?php

                        $my_post_obj = get_post(221); // параметр функции - ID поста, содержимое которого нужно вывести
                        echo $my_post_obj->post_content;
                        ?>

                    </div>
                    <div role="tabpanel" class="tab-pane <?php
                    if($_GET["p"]=="ranch"){
                        echo(" active");
                    }
                    ?>" id="ranch">
                        <?php

                        $my_post_obj = get_post(223); // параметр функции - ID поста, содержимое которого нужно вывести
                        echo $my_post_obj->post_content;
                        ?>

                    </div>
                    <div role="tabpanel" class="tab-pane <?php
                    if($_GET["p"]=="church"){
                        echo(" active");
                    }
                    ?>" id="church">
                        <?php

                        $my_post_obj = get_post(225); // параметр функции - ID поста, содержимое которого нужно вывести
                        echo $my_post_obj->post_content;
                        ?>

                    </div>
                    <div role="tabpanel" class="tab-pane <?php
                    if($_GET["p"]=="pier"){
                        echo(" active");
                    }
                    ?>" id="pier">
                        <?php

                        $my_post_obj = get_post(227); // параметр функции - ID поста, содержимое которого нужно вывести
                        echo $my_post_obj->post_content;
                        ?>

                    </div>
                </div>

            </div>
        </div>

    </div>

</div>
<!-- INFRASTRUCTURE END -->
<?php

get_footer();

?>
