<?php
require_once("scripts/main.php");
?>
<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Cyprus</title>
    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/fonts.css">

    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/index.css">

    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/bootstrap.css">

    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/jquery-2.1.3.min.js" type="text/javascript"
            charset="utf-8"></script>
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/jquery-ui.min.js" type="text/javascript"
            charset="utf-8"></script>
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/bootstrap.min.js" type="text/javascript"
            charset="utf-8"></script>

    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/feature-carousel.css"
          charset="utf-8"/>

    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/jquery-1.7.min.js" type="text/javascript"
            charset="utf-8"></script>
    <script src="<?php echo esc_url(get_template_directory_uri()); ?>/js/jquery.featureCarousel.js"
            type="text/javascript" charset="utf-8"></script>

    <script type="text/javascript" src="<?php echo esc_url(get_template_directory_uri()); ?>/js/pirobox.js"></script>

    <link rel="stylesheet" href="<?php echo esc_url(get_template_directory_uri()); ?>/css/pirobox/gal/style.css">


    <script type="text/javascript">
        $(document).ready(function () {

            $(function(){
                // добавить анимацию к dropdown при открытии //
                $('.dropdown').on('show.bs.dropdown', function(){
                    $(this).find('.dropdown-menu').first().stop(true, true).animate({height: "show"}, 2000);
                });
                // добавить анимацию к dropdown при закрытии //
                $('.dropdown').on('hide.bs.dropdown', function(){
                    $(this).find('.dropdown-menu').first().stop(true, true).animate({height: "hide"}, 2000);
                });
            });

            $().piroBox({
                my_speed: 400, //animation speed
                bg_alpha: 0.1, //background opacity
                slideShow: true, // true == slideshow on, false == slideshow off
                slideSpeed: 4, //slideshow duration in seconds(3 to 6 Recommended)
                close_all: '.piro_close,.piro_overlay'// add class .piro_overlay(with comma)if you want overlay click close piroBox

            });

            window.lfw = 870;
            window.lfh = 500;
            window.sfw = 600;
            window.sfh = 400;

            /*if (window.screen.width >= 1200) {
             window.lfw = 870;
             window.lfh = 500;
             window.sfw = 600;
             window.sfh = 400;
             } else if (window.screen.width >= 992) {
             window.lfw = 719;
             window.lfh = 413;
             window.sfw = 496;
             window.sfh = 330;
             } else if (window.screen.width >= 768) {
             window.lfw = 556;
             window.lfh = 320;
             window.sfw = 384;
             window.sfh = 256;
             } else {
             window.lfw = 464;
             window.lfh = 266;
             window.sfw = 320;
             window.sfh = 213;
             }*/
            window.carousel = $("#carousel").featureCarousel({
                autoPlay: 0,
                largeFeatureWidth: window.lfw,
                largeFeatureHeight: window.lfh,
                smallFeatureWidth: window.sfw,
                smallFeatureHeight: window.sfh,
                sidePadding: 0,
                topPadding: 0
            });

            $("#but_prev").click(function () {
                carousel.prev();
            });
            $("#but_pause").click(function () {
                carousel.pause();
            });
            $("#but_start").click(function () {
                carousel.start();
            });
            $("#but_next").click(function () {
                carousel.next();
            });

            /*$(window).resize(function () {
             if (window.screen.width >= 1200) {
             window.lfw = 870;
             window.lfh = 500;
             window.sfw = 600;
             window.sfh = 400;
             } else if (window.screen.width >= 992) {
             window.lfw = 719;
             window.lfh = 413;
             window.sfw = 496;
             window.sfh = 330;
             } else if (window.screen.width >= 768) {
             window.lfw = 556;
             window.lfh = 320;
             window.sfw = 384;
             window.sfh = 256;
             } else {
             window.lfw = 464;
             window.lfh = 266;
             window.sfw = 320;
             window.sfh = 213;
             }
             window.carousel = $("#carousel").featureCarousel({
             autoPlay: 0,
             largeFeatureWidth: window.lfw,
             largeFeatureHeight: window.lfh,
             smallFeatureWidth: window.sfw,
             smallFeatureHeight: window.sfh,
             sidePadding: 0,
             topPadding: 0
             });

             $("#but_prev").click(function () {
             carousel.prev();
             });
             $("#but_pause").click(function () {
             carousel.pause();
             });
             $("#but_start").click(function () {
             carousel.start();
             });
             $("#but_next").click(function () {
             carousel.next();
             });
             });*/

            $("#show-all-img-btn").click(function () {
                //$("#img-container").attr();
                $("#img-container").animate({height: "900"}, 1000);
                $("#img-hider").animate({height: "100"}, 1000);
                $("#show-all-img-btn").animate({opacity: "hide", height: "hide"}, 1000);

            });


        });
    </script>
</head>
<body>
<!-- HEADER START-->
<div class="row header">
    <div class="container">
        <div class="col-md-12">
            <div class="row hidden-xs">
                <div class="col-md-2 col-sm-2"><img
                        src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png"></div>
                <div class="col-md-6 col-md-offset-4 col-sm-6 col-sm-offset-4">
                    <div class="row ">
                        <div class="col-md-3 col-sm-3 view360 text-center">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/view360.png" alt="">
                            <p class="smalltext-header">
                                <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/gallery/panorama">Обзор
                                    360</a>
                            </p>
                        </div>
                        <div class="col-md-2 col-sm-2 enter-button text-center">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/key.png" alt="">

                            <p class="smalltext-header">
                                <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/regenter/">Вход</a>
                            </p>

                        </div>
                        <div class="col-md-7 col-sm-7">
                            <span class="phone-header">+7(123)456-78-90</span>
                            <button class="callback-header">Обратный звонок</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 menu">

            <nav class="navbar" role="navigation">
                <div class="container-fluid">
                    <!-- Название компании и кнопка, которая отображается для мобильных устройств группируются для лучшего отображения при свертывание -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle pull-left menu-button-xs" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1">
                            <span class=""></span>
                        </button>
                        <div class="visible-xs text-center">
                            <div class="menu-logo-text">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/menu-logo.png"
                                     alt="">
                            </div>

                            <div class="menu-enter-button">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/menu-key.png"
                                     alt="">
                                <span class="">
                                    <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/regenter/">Вход</a>
                                </span>
                            </div>
                            <div class="menu-view360">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/menu-view.png"
                                     alt="">
                                <span class="">
                                    <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/gallery/panorama">Обзор 360</a>
                                </span>
                            </div>
                        </div>
                    </div>

                    <!-- Группируем ссылки, формы, выпадающее меню и прочие элементы -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <?php
                        //custom_menu('Главное меню');
                        my_nav_menu(array(
                                'menu_class' => 'nav navbar-nav',
                                'walker' => new My_Walker_Nav_Menu
                            )
                        );

                        ?>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>


        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-xs-12 visible-xs text-center">
                    <span class="phone-header-xs">+7 (123) 456-78-90</span>
                    <button class="callback-header-xs">Обратный звонок</button>
                </div>
                <div class="col-md-12 hidden-xs">
                    <fieldset class="fieldset fieldset_special fs-header">
                        <legend class="legend">
                            <span class="legend__inner">
                                <span class="legend__left l-header"></span>
                                <span class="legend__content lead-title">Продажа недвижимости на Кипре</span>
                                <span class="legend__right l-header"></span>
                            </span>
                        </legend>
                        <div class="lead-text text-center">Мы уверены, что купленная недвижимость будет Вам в радость.
                            Поэтому говорим - мы не просто строим и продаем недвижимость, но дарим Вам новый, кипрский,
                            стиль жизни!
                        </div>

                    </fieldset>
                </div>
                <div class="col-xs-12 visible-xs text-center">
                    <p class="lead-title-xs">
                        Продажа недвижимости
                    </p>
                    <p class="lead-title-xs">
                        в райском уголке на Кипре
                    </p>
                    <p class="lead-text">
                        Мы уверены, что купленная недвижимость будет Вам в радость.
                        Поэтому говорим - мы не просто строим и продаем недвижимость, но дарим Вам новый, кипрский,
                        стиль жизни!
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- HEADER END-->
<!-- ABOUT VILLA START-->
<div class="row about_villa hidden-xs">
    <div class="container">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-12">
                    <fieldset class="fieldset fieldset_special ">
                        <legend class="legend">
                            <span class="legend__inner">
                                <span class="legend__left l-ab_villa"></span>
                                <span class="legend__content about_villa-title">О виллах Cap St Georgies</span>
                                <span class="legend__right l-ab_villa"></span>
                            </span>
                        </legend>
                        <div class="about_villa-text">
                            <?php

                            $my_post_obj = get_post(230); // параметр функции - ID поста, содержимое которого нужно вывести
                            echo $my_post_obj->post_content;
                            ?>
                        </div>

                    </fieldset>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- ABOUT VILLA END-->
<!-- VILLA PROJ START-->
<div class="row villa_proj">
    <div class="container">

        <div class="row">
            <fieldset class="fieldset fieldset_special ">
                <legend class="legend">
                            <span class="legend__inner">
                                <span class="legend__left l-ab_villa"></span>
                                <span class="legend__content about_villa-title">Проекты вилл</span>
                                <span class="legend__right l-ab_villa"></span>
                            </span>
                </legend>

                <div class="text-center">
                    <!-- Carousel start-->

                    <div class="carousel-container">
                        <div id="carousel-left">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/arrow-left.png"
                                 alt="">
                        </div>
                        <div id="carousel-right">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/arrow-right.png"
                                 alt="">
                        </div>
                        <div id="carousel">

                            <div class="carousel-feature">
                                <a href="#"><img class="carousel-image" alt="Image Caption"
                                                 src="<?php echo esc_url(get_template_directory_uri()); ?>/images/sample1.png"></a>
                                <div class="carousel-caption">
                                    <div class="villa_proj-number">
                                        12
                                    </div>
                                    <div class="villa_proj-text">
                                        Комнат
                                    </div>
                                    <div class="villa_proj-number">
                                        579 <span>м<sup>2</sup></span>
                                    </div>
                                    <div class="villa_proj-text">
                                        Площадь дома
                                    </div>
                                    <div class="villa_proj-number">
                                        8 <span>га</span>
                                    </div>
                                    <div class="villa_proj-text">
                                        Площадь участка
                                    </div>
                                    <div class="villa_proj-price">
                                        <span>Р</span> 78 195 000
                                    </div>
                                    <div class="villa_proj-text">
                                        Стоимость виллы
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-feature">
                                <a href="#"><img class="carousel-image" alt="Image Caption"
                                                 src="<?php echo esc_url(get_template_directory_uri()); ?>/images/sample2.png"></a>
                                <div class="carousel-caption">
                                    <div class="villa_proj-number">
                                        12
                                    </div>
                                    <div class="villa_proj-text">
                                        Комнат
                                    </div>
                                    <div class="villa_proj-number">
                                        579 <span>м<sup>2</sup></span>
                                    </div>
                                    <div class="villa_proj-text">
                                        Площадь дома
                                    </div>
                                    <div class="villa_proj-number">
                                        8 <span>га</span>
                                    </div>
                                    <div class="villa_proj-text">
                                        Площадь участка
                                    </div>
                                    <div class="villa_proj-price">
                                        <span>Р</span> 78 195 000
                                    </div>
                                    <div class="villa_proj-text">
                                        Стоимость виллы
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-feature">
                                <a href="#"><img class="carousel-image" alt="Image Caption"
                                                 src="<?php echo esc_url(get_template_directory_uri()); ?>/images/sample3.jpg"></a>
                                <div class="carousel-caption">
                                    <div class="villa_proj-number">
                                        12
                                    </div>
                                    <div class="villa_proj-text">
                                        Комнат
                                    </div>
                                    <div class="villa_proj-number">
                                        579 <span>м<sup>2</sup></span>
                                    </div>
                                    <div class="villa_proj-text">
                                        Площадь дома
                                    </div>
                                    <div class="villa_proj-number">
                                        8 <span>га</span>
                                    </div>
                                    <div class="villa_proj-text">
                                        Площадь участка
                                    </div>
                                    <div class="villa_proj-price">
                                        <span>Р</span> 78 195 000
                                    </div>
                                    <div class="villa_proj-text">
                                        Стоимость виллы
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-feature">
                                <a href="#"><img class="carousel-image" alt="Image Caption"
                                                 src="<?php echo esc_url(get_template_directory_uri()); ?>/images/sample4.jpg"></a>
                                <div class="carousel-caption">
                                    <div class="villa_proj-number">
                                        12
                                    </div>
                                    <div class="villa_proj-text">
                                        Комнат
                                    </div>
                                    <div class="villa_proj-number">
                                        579 <span>м<sup>2</sup></span>
                                    </div>
                                    <div class="villa_proj-text">
                                        Площадь дома
                                    </div>
                                    <div class="villa_proj-number">
                                        8 <span>га</span>
                                    </div>
                                    <div class="villa_proj-text">
                                        Площадь участка
                                    </div>
                                    <div class="villa_proj-price">
                                        <span>Р</span> 78 195 000
                                    </div>
                                    <div class="villa_proj-text">
                                        Стоимость виллы
                                    </div>
                                </div>
                            </div>
                            <div class="carousel-feature">
                                <a href="#"><img class="carousel-image" alt="Image Caption"
                                                 src="<?php echo esc_url(get_template_directory_uri()); ?>/images/sample5.jpg"></a>
                                <div class="carousel-caption">
                                    <div class="villa_proj-number">
                                        12
                                    </div>
                                    <div class="villa_proj-text">
                                        Комнат
                                    </div>
                                    <div class="villa_proj-number">
                                        579 <span>м<sup>2</sup></span>
                                    </div>
                                    <div class="villa_proj-text">
                                        Площадь дома
                                    </div>
                                    <div class="villa_proj-number">
                                        8 <span>га</span>
                                    </div>
                                    <div class="villa_proj-text">
                                        Площадь участка
                                    </div>
                                    <div class="villa_proj-price">
                                        <span>Р</span> 78 195 000
                                    </div>
                                    <div class="villa_proj-text">
                                        Стоимость виллы
                                    </div>
                                </div>
                            </div>

                        </div>


                    </div>
                    <!-- Carousel end-->
                    <div>
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/see-all.png" alt="">
                        <button class="see-all-btn">Смотреть все проекты</button>
                    </div>
                </div>

            </fieldset>
        </div>

    </div>
</div>
<!-- VILLA PROJ END-->
<!-- INFRASTRUCTURE START -->
<div class="row">
    <div class="container">
        <div>
            <span class="legend__inner">
                                <span class="legend__left l-ab_villa"></span>
                                <span class="legend__content about_villa-title">Инфраструктура</span>
                                <span class="legend__right l-ab_villa"></span>
                            </span>
        </div>
        <div class="infrastructure hidden-xs">

            <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/infrastructure/outer/?p=school" class="school">
                <span class="infrastructure-smalltext text-center">Английская школа</span>
            </a>
            <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/infrastructure/outer/?p=zoo" class="zoo">
                <span class="infrastructure-smalltext text-center">Зоопарк</span>
            </a>
            <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/infrastructure/outer/?p=beach" class="beach">
                <span class="infrastructure-smalltext text-center">Пляж</span>
            </a>
            <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/infrastructure/outer/?p=supermarket"
               class="supermarket">
                <span class="infrastructure-smalltext text-center">Супермаркет</span>
            </a>

            <div class="inner-infrastructure">

                <div class="l-block text-center">
                    <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/infrastructure/inner/?p=reception"
                       class="reception text-center">
                        <span class="reception-pic"></span>
                        <p>24/7 Ресепшн</p>
                    </a>
                    <div class="l-inner-infrastructure"></div>

                </div>
                <div class="l-block">
                    <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/infrastructure/inner/?p=security"
                       class="security text-center">
                        <span class="security-pic"></span>
                        <p>Охрана</p>
                    </a>
                    <div class="l-inner-infrastructure"></div>

                </div>
                <div class="l-block">
                    <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/infrastructure/inner/?p=clubhouse"
                       class="disco text-center">
                        <span class="disco-pic"></span>
                        <p>Клаб-хаус</p>
                    </a>
                    <div class="l-inner-infrastructure"></div>

                </div>
                <div class="l-block">
                    <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/infrastructure/inner/?p=spa"
                       class="spa text-center">
                        <span class="spa-pic"></span>
                        <p>СПА</p>
                    </a>
                    <div class="l-inner-infrastructure"></div>

                </div>

                <!-- CENTER -->
                <div class="house-block">

                </div>
                <!-- CENTER -->

                <div class="l-block text-center">
                    <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/infrastructure/inner/?p=sport"
                       class="sport text-center">
                        <span class="sport-pic"></span>
                        <p>Спортивная зона</p>
                    </a>
                    <div class="r-inner-infrastructure"></div>

                </div>
                <div class="l-block">
                    <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/infrastructure/inner/?p=auditorium"
                       class="auditorium text-center">
                        <span class="auditorium-pic"></span>
                        <p>Конференц зал</p>
                    </a>
                    <div class="r-inner-infrastructure"></div>

                </div>
                <div class="l-block">
                    <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/infrastructure/inner/?p=cinemahall"
                       class="cinemahall text-center">
                        <span class="cinemahall-pic"></span>
                        <p>Кинозал</p>
                    </a>
                    <div class="r-inner-infrastructure"></div>

                </div>
                <div class="l-block">
                    <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/infrastructure/inner/?p=caffe"
                       class="caffe text-center">
                        <span class="caffe-pic"></span>
                        <p>Ресторан</p>
                    </a>
                    <div class="r-inner-infrastructure"></div>

                </div>
                <div class="l-block">
                    <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/infrastructure/inner/?p=helipad"
                       class="helipad text-center">
                        <span class="helipad-pic"></span>
                        <p>Вертолётная площадка</p>
                    </a>
                    <div class="r-inner-infrastructure"></div>

                </div>

            </div>

            <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/infrastructure/outer/?p=reserve"
               class="reserve">
                <span class="infrastructure-smalltext text-center">Заповедник</span>
            </a>
            <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/infrastructure/outer/?p=ranch" class="ranch">
                <span class="infrastructure-smalltext text-center">Ранчо</span>
            </a>
            <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/infrastructure/outer/?p=church" class="church">
                <span class="infrastructure-smalltext text-center">Храм</span>
            </a>
            <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/infrastructure/outer/?p=pier" class="pier">
                <span class="infrastructure-smalltext text-center">Причал для яхт</span>
            </a>
        </div>

        <div class="infrastructure-xs visible-xs">
            <div class="col-xs-12">
                <div class="inf-xs-title text-center">
                    <img
                        src="<?php echo esc_url(get_template_directory_uri()); ?>/images/infrastructure-xs/inner/inner.png"
                        alt="">
                    <a href="#">Внутренняя</a>
                </div>
                <div class="inner-inf-left-main">
                    <div class="inner-inf-left-xs ">
                        <div class="l-inner-infrastructure-xs"></div>

                        <a class="security-xs" href="#">
                            <span>Охрана</span>
                        </a>
                    </div>

                    <div class="inner-inf-left-xs ">
                        <div class="l-inner-infrastructure-xs"></div>

                        <a class="spa-xs" href="#">
                            <span>СПА</span>
                        </a>
                    </div>
                    <div class="inner-inf-left-xs ">
                        <div class="l-inner-infrastructure-xs"></div>

                        <a class="auditorium-xs" href="#">
                            <span>Конференц зал</span>
                        </a>
                    </div>
                    <div class="inner-inf-left-xs ">
                        <div class="l-inner-infrastructure-xs"></div>

                        <a class="caffe-xs" href="#">
                            <span>Ресторан</span>
                        </a>
                    </div>
                </div>
                <div class="inner-inf-right-main">
                    <div class="inner-inf-right-xs ">
                        <div class="r-inner-infrastructure-xs"></div>

                        <a class="reception-xs" href="#">
                            <span>24/7 Ресепшн</span>
                        </a>
                    </div>
                    <div class="inner-inf-right-xs ">
                        <div class="r-inner-infrastructure-xs"></div>

                        <a class="disco-xs" href="#">
                            <span>Клаб-хаус</span>
                        </a>
                    </div>
                    <div class="inner-inf-right-xs ">
                        <div class="r-inner-infrastructure-xs"></div>

                        <a class="sport-xs" href="#">
                            <span>Спортивная зона</span>
                        </a>
                    </div>
                    <div class="inner-inf-right-xs ">
                        <div class="r-inner-infrastructure-xs"></div>

                        <a class="cinemahall-xs" href="#">
                            <span>Кинотеатр</span>
                        </a>
                    </div>
                    <div class="inner-inf-right-xs ">
                        <div class="r-inner-infrastructure-xs"></div>

                        <a class="helipad-xs" href="#">
                            <span>Вертолётная площадка</span>
                        </a>
                    </div>
                </div>
            </div>


            <div class="col-xs-12">
                <div class="inf-xs-title text-center">
                    <img
                        src="<?php echo esc_url(get_template_directory_uri()); ?>/images/infrastructure-xs/outer/outer.png"
                        alt="">
                    <a href="#">Внешняя</a>
                </div>
                <div class="outer-inf-left-main">
                    <div class="outer-inf-left-xs ">
                        <div class="l-outer-infrastructure-xs"></div>
                        <a class="zoo-xs" href="#">
                            <span>Зоопарк</span>
                        </a>
                    </div>
                    <div class="outer-inf-left-xs ">
                        <div class="l-outer-infrastructure-xs"></div>
                        <a class="supermarket-xs" href="#">
                            <span>Супермаркет</span>
                        </a>
                    </div>
                    <div class="outer-inf-left-xs ">
                        <div class="l-outer-infrastructure-xs"></div>
                        <a class="ranch-xs" href="#">
                            <span>Ранчо</span>
                        </a>
                    </div>
                    <div class="outer-inf-left-xs ">
                        <div class="l-outer-infrastructure-xs"></div>
                        <a class="pier-xs" href="#">
                            <span>Причал для яхт</span>
                        </a>
                    </div>
                </div>
                <div class="outer-inf-right-main">
                    <div class="outer-inf-right-xs ">
                        <div class="r-outer-infrastructure-xs"></div>
                        <a class="school-xs" href="#">
                            <span>Английская школа</span>
                        </a>
                    </div>
                    <div class="outer-inf-right-xs ">
                        <div class="r-outer-infrastructure-xs"></div>
                        <a class="beach-xs" href="#">
                            <span>Пляж</span>
                        </a>
                    </div>
                    <div class="outer-inf-right-xs ">
                        <div class="r-outer-infrastructure-xs"></div>
                        <a class="zoo-xs" href="#">
                            <span>Заповедник</span>
                        </a>
                    </div>
                    <div class="outer-inf-right-xs ">
                        <div class="r-outer-infrastructure-xs"></div>
                        <a class="church-xs" href="#">
                            <span>Храм</span>
                        </a>
                    </div>
                </div>
            </div>

        </div>
    </div>

</div>
<!-- INFRASTRUCTURE END -->
<!-- ABOUT COMPANY START -->
<div class="row about-company hidden-xs">
    <div class="container">
        <div class="row ">
            <div>
                <fieldset class="fieldset fieldset_special about-company-desc">
                    <legend class="legend">
                            <span class="legend__inner">
                                <span class="legend__left "></span>
                                <span class="legend__content about-company-desctitle">О компании</span>
                                <span class="legend__right "></span>
                            </span>
                    </legend>

                    <div class="about-company-desctext">
                        <?php

                        $my_post_obj = get_post(232); // параметр функции - ID поста, содержимое которого нужно вывести
                        echo $my_post_obj->post_content;
                        ?>
                    </div>

                </fieldset>
            </div>
        </div>

        <div class="row" style="margin-bottom: 50px;">
            <div class="col-md-2">
                <img style="margin-top:100%;" class="hidden-sm hidden-xs"
                     src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo2.png"
                     alt="">
            </div>
            <div class="col-md-10">
                <div class="row">
                    <span class="about-company-bot-title">
                    Продажа недвижимости на Кипре
                    </span>
                </div>
                <div class="row">
                    <span class="about-company-bot-text">
                         <?php

                         $my_post_obj = get_post(234); // параметр функции - ID поста, содержимое которого нужно вывести
                         echo $my_post_obj->post_content;
                         ?>

                    </span>
                </div>
            </div>

        </div>
    </div>
</div>
<div class="row about-company-xs visible-xs">
    <div class="col-xs-12 about-company-xs-title">Продажа недвижимости на Кипре</div>
    <div class="col-xs-12 about-company-xs-text">
        <?php

        $my_post_obj = get_post(238); // параметр функции - ID поста, содержимое которого нужно вывести
        echo $my_post_obj->post_content;
        ?>
    </div>
</div>
<!-- ABOUT COMPANY END -->
<!-- GALLERY START -->
<div class="row gallery">
    <div class="container">
        <div class="row">

                            <span class="legend__inner">
                                <span class="legend__left l-ab_villa"></span>
                                <span class="legend__content about_villa-title">Галерея</span>
                                <span class="legend__right l-ab_villa"></span>
                            </span>

        </div>

    </div>
    <div id="img-container" class="col-md-12 col-xs-12">
        <div id="wrapper">
            <?php echo photo_gallery(14); ?>
        </div>
        <div id="img-hider">
            <div class="text-center">
                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/see-all.png" alt=""><br>
                <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/gallery/photo/" class="see-all-gal-btn">смотреть
                    всю галерею</a><br>
                <button id="show-all-img-btn" class="hidden-xs"></button>
            </div>
        </div>
    </div>


</div>
<!-- GALLERY END -->
<!-- NEWS START -->
<div class="row news">
    <div class="container">

        <div class="row hidden-xs">
            <div class="col-md-12">
                <span class="about_villa-title">Новости</span>
            </div>
        </div>

        <div class="row visible-xs">
            <div class="col-md-12">
                <span class="legend__inner">
                    <span class="legend__left l-ab_villa "></span>
                    <span class="legend__content about_villa-title">Новости</span>
                    <span class="legend__right l-ab_villa "></span>
                </span>
            </div>
        </div>

        <div class="row news-list">
            <?php

            $res = get_all_news($wpdb);

            if (isset($res)) {

                foreach ($res as $news) {
                    $date = date("d.m.Y", strtotime($news->date));
                    ?>

                    <div class="news-block col-md-3 col-sm-6 col-xs-12">
                        <img src="http://<?php echo $_SERVER["SERVER_NAME"] ?><?php echo $news->image ?>"/>
                        <p class="news-date"><?php echo $date ?></p>
                        <p class="news-title"><?php echo $news->title ?></p>
                        <p class="news-text">
                            <?php
                            $text =  $news->content;
                            $text_length = 200;
                            if (strlen( $news->content)>$text_length)
                            {
                            $text = substr ($text, 0,strpos ($text, " ", $text_length)); echo $text."...";
                            }
                            else echo $text;
                            ?>
                        </p>
                    </div>
                    <?php
                }
            }
            ?>

        </div>

        <div class="row">
            <div class="col-md-6 col-md-offset-6 form-order">
                <div class="form-inner-container">
                    <p class="form-title">Заказать ознакомительный тур</p>

                    <div class="input-container">
                        <form class="">
                            <input type="text" placeholder="Ваш телефон" class="form-input">
                            <input type="button" class="form-button">
                        </form>
                    </div>


                    <p class="form-text hidden-xs">
                        <?php

                        $my_post_obj = get_post(236); // параметр функции - ID поста, содержимое которого нужно вывести
                        echo $my_post_obj->post_content;
                        ?>
                    </p>
                </div>

            </div>
        </div>

    </div>
    <!-- FOOTER START -->
    <div class="footer">
        <div class="container footer-container text-center">
            <div class="row">
                <div class="col-md-1 col-xs-12 footer-big-logo">
                    <img class="" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-footer.png"
                         alt="">
                </div>
                <div class="footer-logo col-md-4 col-xs-12 ">
                    <img class="" src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo-text-footer.png"
                         alt="">
                    <p class="hidden-xs copyright">С 2016 | Cap St Georges. Все права защищены</p>
                </div>
                <div class="col-md-3 col-xs-12">
                    <span class="footer-phone">+7(123)456-78-90</span><br>
                    <span class="footer-smalltext">E-mail: info@namecompany.ru</span><br>
                    <span class="footer-smalltext">Адрес: г.Москва, ул.Толстого 179</span>
                </div>
                <div class="hidden-xs col-md-3 col-md-offset-1 col-xs-12" style="margin-top: 50px;">
                    <div style="margin-left: 50px;">
                        <a href="http://<?php echo(get_option('vk_link')) ?>">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/social_vk.png""
                            alt="">
                        </a>
                        <a href="http://<?php echo(get_option('instagram_link')) ?>">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/social_instagram.png""
                            alt="">
                        </a>
                        <a href="http://<?php echo(get_option('watsapp_link')) ?>">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/social_watsapp.png""
                            alt="">
                        </a>
                        <a href="http://<?php echo(get_option('viber_link')) ?>">
                            <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/social_viber.png""
                            alt="">
                        </a>
                    </div>
                    <div class="">Разработано в <a href="<?php echo(get_option('adtherapy_link')) ?>">Рекламотерапия</a>
                    </div>
                </div>
                <div>
                    <p class="visible-xs copyright">С 2016 | Cap St Georges. Все права защищены</p>
                </div>
            </div>

        </div>
    </div>
    <!-- FOOTER END -->
</div>
<!-- NEWS END -->

</body>
</html>