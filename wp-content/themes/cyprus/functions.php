<?php

define('SHORTINIT', true);
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');

function wptuts_scripts_basic()
{
    wp_enqueue_style("bootstrap", get_template_directory_uri() . "/css/bootstrap.css");
    wp_enqueue_script("jquery-2.1.3.min.js", get_template_directory_uri() . "/js/jquery-2.1.3.min.js");
    wp_enqueue_script("jquery-ui.min.js", get_template_directory_uri() . "/js/jquery-ui.min.js");
    wp_enqueue_script("bootstrap.min.js", get_template_directory_uri() . "/js/bootstrap.min.js");


    wp_enqueue_style("fonts", get_template_directory_uri() . "/css/fonts.css");
    wp_enqueue_style("header", get_template_directory_uri() . "/css/header.css");
    wp_enqueue_style("footer", get_template_directory_uri() . "/css/footer.css");

    $page_ID = get_the_ID();

    if ($page_ID == 1){
        wp_enqueue_style("feature-carousel", get_template_directory_uri() . "/css/feature-carousel.css");
        wp_enqueue_script("jquery-1.7.min.js", get_template_directory_uri() . "/js/jquery-1.7.min.js");
        wp_enqueue_script("jquery.featureCarousel.js", get_template_directory_uri() . "/js/jquery.featureCarousel.js");
        wp_enqueue_script("main", get_template_directory_uri() . "/js/main.js");
    }
    if ($page_ID == 17 || $page_ID == 22 || $page_ID == 26 || $page_ID == 11) {
        wp_enqueue_style("citizenship", get_template_directory_uri() . "/css/citizenship.css");
    }
    if ($page_ID == 78 || $page_ID == 80 ) {
        wp_enqueue_style("contact", get_template_directory_uri() . "/css/contact.css");
    }
    if ($page_ID == 65 || $page_ID == 63 ) {
        wp_enqueue_style("infrastructure", get_template_directory_uri() . "/css/infrastructure.css");
    }
    if ($page_ID == 69 || $page_ID == 71 || $page_ID == 73 ) {
        wp_enqueue_style("gallery", get_template_directory_uri() . "/css/gallery.css");
    }
    if ($page_ID == 52 || $page_ID == 54 || $page_ID == 56 || $page_ID == 58 ) {
        wp_enqueue_style("villaproject", get_template_directory_uri() . "/css/villaproject.css");
    }
    if ($page_ID == 46 || $page_ID == 44 || $page_ID == 56 || $page_ID == 48 ) {
        wp_enqueue_style("masterplan", get_template_directory_uri() . "/css/masterplan.css");
    }
    if ($page_ID == 31 || $page_ID == 33 || $page_ID == 35 || $page_ID == 37 || $page_ID == 39) {
        wp_enqueue_style("aboutcyprus", get_template_directory_uri() . "/css/about.css");
        wp_enqueue_script("map", get_template_directory_uri() . "/js/map.js");
    }
    if ($page_ID == 182 || $page_ID == 184) {
        wp_enqueue_style("agent", get_template_directory_uri() . "/css/agent.css");
        wp_enqueue_script("account.js", get_template_directory_uri() . "/js/account.js");
        wp_localize_script( 'account.js',   'acc_ajax',   array( 'url' => get_template_directory_uri().'/scripts/account.php' ));
    }
}

add_action('wp_enqueue_scripts', 'wptuts_scripts_basic');

function theme_register_nav_menu()
{
    register_nav_menus(array(
        'main_menu' => 'Главное меню',
    ));
}

add_action('after_setup_theme', 'theme_register_nav_menu');

class My_Walker_Nav_Menu extends Walker
{
    /**
     * What the class handles.
     *
     * @see Walker::$tree_type
     * @since 3.0.0
     * @var string
     */
    public $tree_type = array('post_type', 'taxonomy', 'custom');

    /**
     * Database fields to use.
     *
     * @see Walker::$db_fields
     * @since 3.0.0
     * @todo Decouple this.
     * @var array
     */
    public $db_fields = array('parent' => 'menu_item_parent', 'id' => 'db_id');

    /**
     * Starts the list before the elements are added.
     *
     * @see Walker::start_lvl()
     *
     * @since 3.0.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param int $depth Depth of menu item. Used for padding.
     * @param array $args An array of arguments. @see wp_nav_menu()
     */
    public function start_lvl(&$output, $depth = 0, $args = array())
    {
        $indent = str_repeat("\t", $depth);
        $output .= "\n$indent<ul class=\"dropdown-menu\">\n";
    }

    /**
     * Ends the list of after the elements are added.
     *
     * @see Walker::end_lvl()
     *
     * @since 3.0.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param int $depth Depth of menu item. Used for padding.
     * @param array $args An array of arguments. @see wp_nav_menu()
     */
    public function end_lvl(&$output, $depth = 0, $args = array())
    {
        $indent = str_repeat("\t", $depth);
        $output .= "$indent</ul>\n";
    }

    /**
     * Start the element output.
     *
     * @see Walker::start_el()
     *
     * @since 3.0.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item Menu item data object.
     * @param int $depth Depth of menu item. Used for padding.
     * @param array $args An array of arguments. @see wp_nav_menu()
     * @param int $id Current item ID.
     */
    public function start_el(&$output, $item, $depth = 0, $args = array(), $id = 0)
    {
        $indent = ($depth) ? str_repeat("\t", $depth) : '';

        $classes = empty($item->classes) ? array() : (array)$item->classes;
        $classes[] = '';

        /**
         * Filter the CSS class(es) applied to a menu item's <li>.
         *
         * @since 3.0.0
         *
         * @see wp_nav_menu()
         *
         * @param array $classes The CSS classes that are applied to the menu item's <li>.
         * @param object $item The current menu item.
         * @param array $args An array of wp_nav_menu() arguments.
         */
        $class_names = join(' ', apply_filters('nav_menu_css_class', array_filter($classes), $item, $args));
        $class_names = $class_names ? ' class="' . esc_attr($class_names) . '"' : '';

        /**
         * Filter the ID applied to a menu item's <li>.
         *
         * @since 3.0.1
         *
         * @see wp_nav_menu()
         *
         * @param string $menu_id The ID that is applied to the menu item's <li>.
         * @param object $item The current menu item.
         * @param array $args An array of wp_nav_menu() arguments.
         */
        $id = apply_filters('nav_menu_item_id', '', $item, $args);
        $id = $id ? ' id="' . esc_attr($id) . '"' : '';

        $output .= $indent . '<li' . $id . $class_names . '>';

        $atts = array();
        $atts['title'] = !empty($item->attr_title) ? $item->attr_title : '';
        $atts['target'] = !empty($item->target) ? $item->target : '';
        $atts['rel'] = !empty($item->xfn) ? $item->xfn : '';
        if ($item->post_parent != 0) {
            $atts['href'] = !empty($item->url) ? $item->url : '';
        } else {
            $atts['href'] = '#';
            $atts['class'] = 'dropdown-toggle';
            $atts['data-toggle'] = 'dropdown';
            $atts['aria-expanded'] = 'false';
        }


        /**
         * Filter the HTML attributes applied to a menu item's <a>.
         *
         * @since 3.6.0
         *
         * @see wp_nav_menu()
         *
         * @param array $atts {
         *     The HTML attributes applied to the menu item's <a>, empty strings are ignored.
         *
         * @type string $title Title attribute.
         * @type string $target Target attribute.
         * @type string $rel The rel attribute.
         * @type string $href The href attribute.
         * }
         * @param object $item The current menu item.
         * @param array $args An array of wp_nav_menu() arguments.
         */
        $atts = apply_filters('nav_menu_link_attributes', $atts, $item, $args);

        $attributes = '';
        foreach ($atts as $attr => $value) {
            if (!empty($value)) {
                $value = ('href' === $attr) ? esc_url($value) : esc_attr($value);
                $attributes .= ' ' . $attr . '="' . $value . '"';
            }
        }

        $item_output = $args->before;
        $item_output .= '<a' . $attributes . '>';
        /** This filter is documented in wp-includes/post-template.php */
        $item_output .= $args->link_before . apply_filters('the_title', $item->title, $item->ID) . $args->link_after;
        if ($item->post_parent == 0) {
            $item_output .= '<b class="menu_switcher visible-xs"></b></a>';
        } else {
            $item_output .= '</a>';
        }

        $item_output .= $args->after;

        /**
         * Filter a menu item's starting output.
         *
         * The menu item's starting output only includes $args->before, the opening <a>,
         * the menu item's title, the closing </a>, and $args->after. Currently, there is
         * no filter for modifying the opening and closing <li> for a menu item.
         *
         * @since 3.0.0
         *
         * @see wp_nav_menu()
         *
         * @param string $item_output The menu item's starting HTML output.
         * @param object $item Menu item data object.
         * @param int $depth Depth of menu item. Used for padding.
         * @param array $args An array of wp_nav_menu() arguments.
         */
        $output .= apply_filters('walker_nav_menu_start_el', $item_output, $item, $depth, $args);
    }

    /**
     * Ends the element output, if needed.
     *
     * @see Walker::end_el()
     *
     * @since 3.0.0
     *
     * @param string $output Passed by reference. Used to append additional content.
     * @param object $item Page data object. Not used.
     * @param int $depth Depth of page. Not Used.
     * @param array $args An array of arguments. @see wp_nav_menu()
     */
    public function end_el(&$output, $item, $depth = 0, $args = array())
    {
        $output .= "</li>\n";
    }

}

/**
 * Displays a navigation menu.
 *
 * @since 3.0.0
 *
 * @param array $args {
 *     Optional. Array of nav menu arguments.
 *
 * @type string $menu Desired menu. Accepts (matching in order) id, slug, name. Default empty.
 * @type string $menu_class CSS class to use for the ul element which forms the menu. Default 'menu'.
 * @type string $menu_id The ID that is applied to the ul element which forms the menu.
 *                                          Default is the menu slug, incremented.
 * @type string $container Whether to wrap the ul, and what to wrap it with. Default 'div'.
 * @type string $container_class Class that is applied to the container. Default 'menu-{menu slug}-container'.
 * @type string $container_id The ID that is applied to the container. Default empty.
 * @type callback|bool $fallback_cb If the menu doesn't exists, a callback function will fire.
 *                                          Default is 'wp_page_menu'. Set to false for no fallback.
 * @type string $before Text before the link text. Default empty.
 * @type string $after Text after the link text. Default empty.
 * @type string $link_before Text before the link. Default empty.
 * @type string $link_after Text after the link. Default empty.
 * @type bool $echo Whether to echo the menu or return it. Default true.
 * @type int $depth How many levels of the hierarchy are to be included. 0 means all. Default 0.
 * @type object $walker Instance of a custom walker class. Default empty.
 * @type string $theme_location Theme location to be used. Must be registered with register_nav_menu()
 *                                          in order to be selectable by the user.
 * @type string $items_wrap How the list items should be wrapped. Default is a ul with an id and class.
 *                                          Uses printf() format with numbered placeholders.
 * }
 * @return mixed Menu output if $echo is false, false if there are no items or no menu was found.
 */
function my_nav_menu($args = array())
{
    static $menu_id_slugs = array();

    $defaults = array('menu' => '', 'container' => 'div', 'container_class' => '', 'container_id' => '', 'menu_class' => 'menu', 'menu_id' => '',
        'echo' => true, 'fallback_cb' => 'wp_page_menu', 'before' => '', 'after' => '', 'link_before' => '', 'link_after' => '', 'items_wrap' => '<ul id="%1$s" class="%2$s">%3$s</ul>',
        'depth' => 0, 'walker' => '', 'theme_location' => '');

    $args = wp_parse_args($args, $defaults);
    /**
     * Filter the arguments used to display a navigation menu.
     *
     * @since 3.0.0
     *
     * @see wp_nav_menu()
     *
     * @param array $args Array of wp_nav_menu() arguments.
     */
    $args = apply_filters('wp_nav_menu_args', $args);
    $args = (object)$args;

    /**
     * Filter whether to short-circuit the wp_nav_menu() output.
     *
     * Returning a non-null value to the filter will short-circuit
     * wp_nav_menu(), echoing that value if $args->echo is true,
     * returning that value otherwise.
     *
     * @since 3.9.0
     *
     * @see wp_nav_menu()
     *
     * @param string|null $output Nav menu output to short-circuit with. Default null.
     * @param object $args An object containing wp_nav_menu() arguments.
     */
    $nav_menu = apply_filters('pre_wp_nav_menu', null, $args);

    if (null !== $nav_menu) {
        if ($args->echo) {
            echo $nav_menu;
            return;
        }

        return $nav_menu;
    }

    // Get the nav menu based on the requested menu
    $menu = wp_get_nav_menu_object($args->menu);

    // Get the nav menu based on the theme_location
    if (!$menu && $args->theme_location && ($locations = get_nav_menu_locations()) && isset($locations[$args->theme_location]))
        $menu = wp_get_nav_menu_object($locations[$args->theme_location]);

    // get the first menu that has items if we still can't find a menu
    if (!$menu && !$args->theme_location) {
        $menus = wp_get_nav_menus(array('orderby' => 'name'));
        foreach ($menus as $menu_maybe) {
            if ($menu_items = wp_get_nav_menu_items($menu_maybe->term_id, array('update_post_term_cache' => false))) {
                $menu = $menu_maybe;
                break;
            }
        }
    }

    // If the menu exists, get its items.
    if ($menu && !is_wp_error($menu) && !isset($menu_items))
        $menu_items = wp_get_nav_menu_items($menu->term_id, array('update_post_term_cache' => false));

    /*
     * If no menu was found:
     *  - Fall back (if one was specified), or bail.
     *
     * If no menu items were found:
     *  - Fall back, but only if no theme location was specified.
     *  - Otherwise, bail.
     */
    if ((!$menu || is_wp_error($menu) || (isset($menu_items) && empty($menu_items) && !$args->theme_location))
        && $args->fallback_cb && is_callable($args->fallback_cb)
    )
        return call_user_func($args->fallback_cb, (array)$args);

    if (!$menu || is_wp_error($menu))
        return false;

    $nav_menu = $items = '';

    $show_container = false;
    if ($args->container) {
        /**
         * Filter the list of HTML tags that are valid for use as menu containers.
         *
         * @since 3.0.0
         *
         * @param array $tags The acceptable HTML tags for use as menu containers.
         *                    Default is array containing 'div' and 'nav'.
         */
        $allowed_tags = apply_filters('wp_nav_menu_container_allowedtags', array('div', 'nav'));
        if (in_array($args->container, $allowed_tags)) {
            $show_container = true;
            $class = $args->container_class ? ' class="' . esc_attr($args->container_class) . '"' : ' class="menu-' . $menu->slug . '-container"';
            $id = $args->container_id ? ' id="' . esc_attr($args->container_id) . '"' : '';
            $nav_menu .= '<' . $args->container . $id . $class . '>';
        }
    }

    // Set up the $menu_item variables
    _wp_my_menu_item_classes_by_context($menu_items);

    $sorted_menu_items = $menu_items_with_children = array();
    foreach ((array)$menu_items as $menu_item) {
        $sorted_menu_items[$menu_item->menu_order] = $menu_item;
        if ($menu_item->menu_item_parent)
            $menu_items_with_children[$menu_item->menu_item_parent] = true;
    }

    // Add the menu-item-has-children class where applicable
    if ($menu_items_with_children) {
        foreach ($sorted_menu_items as &$menu_item) {
            if (isset($menu_items_with_children[$menu_item->ID]))
                $menu_item->classes[] = 'dropdown';
        }
    }

    unset($menu_items, $menu_item);

    /**
     * Filter the sorted list of menu item objects before generating the menu's HTML.
     *
     * @since 3.1.0
     *
     * @param array $sorted_menu_items The menu items, sorted by each menu item's menu order.
     * @param object $args An object containing wp_nav_menu() arguments.
     */
    $sorted_menu_items = apply_filters('wp_nav_menu_objects', $sorted_menu_items, $args);

    $items .= walk_my_nav_menu_tree($sorted_menu_items, $args->depth, $args);
    unset($sorted_menu_items);

    // Attributes
    if (!empty($args->menu_id)) {
        $wrap_id = $args->menu_id;
    } else {
        //$wrap_id = 'menu-' . $menu->slug;
        $wrap_id = ' ';
        while (in_array($wrap_id, $menu_id_slugs)) {
            if (preg_match('#-(\d+)$#', $wrap_id, $matches))
                $wrap_id = preg_replace('#-(\d+)$#', '-' . ++$matches[1], $wrap_id);
            else
                $wrap_id = $wrap_id . '-1';
        }
    }
    $menu_id_slugs[] = $wrap_id;

    $wrap_class = $args->menu_class ? $args->menu_class : '';

    /**
     * Filter the HTML list content for navigation menus.
     *
     * @since 3.0.0
     *
     * @see wp_nav_menu()
     *
     * @param string $items The HTML list content for the menu items.
     * @param object $args An object containing wp_nav_menu() arguments.
     */
    $items = apply_filters('wp_nav_menu_items', $items, $args);
    /**
     * Filter the HTML list content for a specific navigation menu.
     *
     * @since 3.0.0
     *
     * @see wp_nav_menu()
     *
     * @param string $items The HTML list content for the menu items.
     * @param object $args An object containing wp_nav_menu() arguments.
     */
    $items = apply_filters("wp_nav_menu_{$menu->slug}_items", $items, $args);

    // Don't print any markup if there are no items at this point.
    if (empty($items))
        return false;
    $nav_menu .= "<li class=\"menu_close visible-xs\" data-toggle=\"collapse\" data-target=\"#bs-example-navbar-collapse-1\" aria-expanded=\"true\"><a>Меню</a></li>";
    $nav_menu .= sprintf($args->items_wrap, esc_attr($wrap_id), esc_attr($wrap_class), $items);
    unset($items);

    if ($show_container)
        $nav_menu .= '</' . $args->container . '>';

    /**
     * Filter the HTML content for navigation menus.
     *
     * @since 3.0.0
     *
     * @see wp_nav_menu()
     *
     * @param string $nav_menu The HTML content for the navigation menu.
     * @param object $args An object containing wp_nav_menu() arguments.
     */
    $nav_menu = apply_filters('wp_nav_menu', $nav_menu, $args);

    if ($args->echo)
        echo $nav_menu;
    else
        return $nav_menu;
}

/**
 * Add the class property classes for the current context, if applicable.
 *
 * @access private
 * @since 3.0.0
 *
 * @param array $menu_items The current menu item objects to which to add the class property information.
 */
function _wp_my_menu_item_classes_by_context(&$menu_items)
{
    global $wp_query, $wp_rewrite;

    $queried_object = $wp_query->get_queried_object();
    $queried_object_id = (int)$wp_query->queried_object_id;

    $active_object = '';
    $active_ancestor_item_ids = array();
    $active_parent_item_ids = array();
    $active_parent_object_ids = array();
    $possible_taxonomy_ancestors = array();
    $possible_object_parents = array();
    $home_page_id = (int)get_option('page_for_posts');

    if ($wp_query->is_singular && !empty($queried_object->post_type) && !is_post_type_hierarchical($queried_object->post_type)) {
        foreach ((array)get_object_taxonomies($queried_object->post_type) as $taxonomy) {
            if (is_taxonomy_hierarchical($taxonomy)) {
                $term_hierarchy = _get_term_hierarchy($taxonomy);
                $terms = wp_get_object_terms($queried_object_id, $taxonomy, array('fields' => 'ids'));
                if (is_array($terms)) {
                    $possible_object_parents = array_merge($possible_object_parents, $terms);
                    $term_to_ancestor = array();
                    foreach ((array)$term_hierarchy as $anc => $descs) {
                        foreach ((array)$descs as $desc)
                            $term_to_ancestor[$desc] = $anc;
                    }

                    foreach ($terms as $desc) {
                        do {
                            $possible_taxonomy_ancestors[$taxonomy][] = $desc;
                            if (isset($term_to_ancestor[$desc])) {
                                $_desc = $term_to_ancestor[$desc];
                                unset($term_to_ancestor[$desc]);
                                $desc = $_desc;
                            } else {
                                $desc = 0;
                            }
                        } while (!empty($desc));
                    }
                }
            }
        }
    } elseif (!empty($queried_object->taxonomy) && is_taxonomy_hierarchical($queried_object->taxonomy)) {
        $term_hierarchy = _get_term_hierarchy($queried_object->taxonomy);
        $term_to_ancestor = array();
        foreach ((array)$term_hierarchy as $anc => $descs) {
            foreach ((array)$descs as $desc)
                $term_to_ancestor[$desc] = $anc;
        }
        $desc = $queried_object->term_id;
        do {
            $possible_taxonomy_ancestors[$queried_object->taxonomy][] = $desc;
            if (isset($term_to_ancestor[$desc])) {
                $_desc = $term_to_ancestor[$desc];
                unset($term_to_ancestor[$desc]);
                $desc = $_desc;
            } else {
                $desc = 0;
            }
        } while (!empty($desc));
    }

    $possible_object_parents = array_filter($possible_object_parents);

    $front_page_url = home_url();

    foreach ((array)$menu_items as $key => $menu_item) {

        $menu_items[$key]->current = false;

        $classes = (array)$menu_item->classes;
        $classes[] = '';
        $classes[] = '';
        $classes[] = '';

        // if the menu item corresponds to a taxonomy term for the currently-queried non-hierarchical post object
        if ($wp_query->is_singular && 'taxonomy' == $menu_item->type && in_array($menu_item->object_id, $possible_object_parents)) {
            $active_parent_object_ids[] = (int)$menu_item->object_id;
            $active_parent_item_ids[] = (int)$menu_item->db_id;
            $active_object = $queried_object->post_type;

            // if the menu item corresponds to the currently-queried post or taxonomy object
        } elseif (
            $menu_item->object_id == $queried_object_id &&
            (
                (!empty($home_page_id) && 'post_type' == $menu_item->type && $wp_query->is_home && $home_page_id == $menu_item->object_id) ||
                ('post_type' == $menu_item->type && $wp_query->is_singular) ||
                ('taxonomy' == $menu_item->type && ($wp_query->is_category || $wp_query->is_tag || $wp_query->is_tax) && $queried_object->taxonomy == $menu_item->object)
            )
        ) {
            $classes[] = 'active';
            $menu_items[$key]->current = true;
            $_anc_id = (int)$menu_item->db_id;

            while (
                ($_anc_id = get_post_meta($_anc_id, '_menu_item_menu_item_parent', true)) &&
                !in_array($_anc_id, $active_ancestor_item_ids)
            ) {
                $active_ancestor_item_ids[] = $_anc_id;
            }

            if ('post_type' == $menu_item->type && 'page' == $menu_item->object) {
                // Back compat classes for pages to match wp_page_menu()
                $classes[] = '';
                $classes[] = '';
                $classes[] = '';
            }
            $active_parent_item_ids[] = (int)$menu_item->menu_item_parent;
            $active_parent_object_ids[] = (int)$menu_item->post_parent;
            $active_object = $menu_item->object;

            // if the menu item corresponds to the currently-requested URL
        } elseif ('custom' == $menu_item->object) {
            $_root_relative_current = untrailingslashit($_SERVER['REQUEST_URI']);
            $current_url = set_url_scheme('http://' . $_SERVER['HTTP_HOST'] . $_root_relative_current);
            $raw_item_url = strpos($menu_item->url, '#') ? substr($menu_item->url, 0, strpos($menu_item->url, '#')) : $menu_item->url;
            $item_url = untrailingslashit($raw_item_url);
            $_indexless_current = untrailingslashit(preg_replace('/' . preg_quote($wp_rewrite->index, '/') . '$/', '', $current_url));

            if ($raw_item_url && in_array($item_url, array($current_url, $_indexless_current, $_root_relative_current))) {
                $classes[] = 'active';
                $menu_items[$key]->current = true;
                $_anc_id = (int)$menu_item->db_id;

                while (
                    ($_anc_id = get_post_meta($_anc_id, '_menu_item_menu_item_parent', true)) &&
                    !in_array($_anc_id, $active_ancestor_item_ids)
                ) {
                    $active_ancestor_item_ids[] = $_anc_id;
                }

                if (in_array(home_url(), array(untrailingslashit($current_url), untrailingslashit($_indexless_current)))) {
                    // Back compat for home link to match wp_page_menu()
                    $classes[] = '';
                }
                $active_parent_item_ids[] = (int)$menu_item->menu_item_parent;
                $active_parent_object_ids[] = (int)$menu_item->post_parent;
                $active_object = $menu_item->object;

                // give front page item current-menu-item class when extra query arguments involved
            } elseif ($item_url == $front_page_url && is_front_page()) {
                $classes[] = 'active';
            }

            if (untrailingslashit($item_url) == home_url())
                $classes[] = 'menu-item-home';
        }

        // back-compat with wp_page_menu: add "current_page_parent" to static home page link for any non-page query
        if (!empty($home_page_id) && 'post_type' == $menu_item->type && empty($wp_query->is_page) && $home_page_id == $menu_item->object_id)
            $classes[] = 'current_page_parent';

        $menu_items[$key]->classes = array_unique($classes);
    }
    $active_ancestor_item_ids = array_filter(array_unique($active_ancestor_item_ids));
    $active_parent_item_ids = array_filter(array_unique($active_parent_item_ids));
    $active_parent_object_ids = array_filter(array_unique($active_parent_object_ids));

    // set parent's class
    foreach ((array)$menu_items as $key => $parent_item) {
        $classes = (array)$parent_item->classes;
        $menu_items[$key]->current_item_ancestor = false;
        $menu_items[$key]->current_item_parent = false;

        if (
            isset($parent_item->type) &&
            (
                // ancestral post object
                (
                    'post_type' == $parent_item->type &&
                    !empty($queried_object->post_type) &&
                    is_post_type_hierarchical($queried_object->post_type) &&
                    in_array($parent_item->object_id, $queried_object->ancestors) &&
                    $parent_item->object != $queried_object->ID
                ) ||

                // ancestral term
                (
                    'taxonomy' == $parent_item->type &&
                    isset($possible_taxonomy_ancestors[$parent_item->object]) &&
                    in_array($parent_item->object_id, $possible_taxonomy_ancestors[$parent_item->object]) &&
                    (
                        !isset($queried_object->term_id) ||
                        $parent_item->object_id != $queried_object->term_id
                    )
                )
            )
        ) {
            $classes[] = empty($queried_object->taxonomy) ? 'current-' . $queried_object->post_type . '-ancestor' : 'current-' . $queried_object->taxonomy . '-ancestor';
        }

        if (in_array(intval($parent_item->db_id), $active_ancestor_item_ids)) {
            $classes[] = open;
            $menu_items[$key]->current_item_ancestor = true;
        }
        if (in_array($parent_item->db_id, $active_parent_item_ids)) {
            $classes[] = 'current-menu-parent';
            $menu_items[$key]->current_item_parent = true;
        }
        if (in_array($parent_item->object_id, $active_parent_object_ids))
            $classes[] = 'current-' . $active_object . '-parent';

        if ('post_type' == $parent_item->type && 'page' == $parent_item->object) {
            // Back compat classes for pages to match wp_page_menu()
            if (in_array('current-menu-parent', $classes))
                $classes[] = 'current_page_parent';
            if (in_array(open1, $classes))
                $classes[] = 'current_page_ancestor';
        }

        $menu_items[$key]->classes = array_unique($classes);
    }
}

/**
 * Retrieve the HTML list content for nav menu items.
 *
 * @uses Walker_Nav_Menu to create HTML list content.
 * @since 3.0.0
 * @see  Walker::walk() for parameters and return description.
 */
function walk_my_nav_menu_tree($items, $depth, $r)
{
    $walker = (empty($r->walker)) ? new Walker_Nav_Menu : $r->walker;
    $args = array($items, $depth, $r);

    return call_user_func_array(array($walker, 'walk'), $args);
}

/**
 * Prevents a menu item ID from being used more than once.
 *
 * @since 3.0.1
 * @access private
 */
function _nav_my_menu_item_id_use_once($id, $item)
{
    static $_used_ids = array();
    if (in_array($item->ID, $_used_ids))
        return '';
    $_used_ids[] = $item->ID;
    return $id;
}

add_filter('nav_menu_item_id', '_nav_my_menu_item_id_use_once', 10, 2);

function mytheme_customize_register( $wp_customize ) {


    /*
    Добавляем секцию в настройки темы
    */
    $wp_customize->add_section(
    // ID
        'data_header_section',
        // Arguments array
        array(
            'title' => 'Данные шапки сайта',
            'capability' => 'edit_theme_options',
            'description' => "Тут можно указать данные сайта"
        )
    );

    /*
    Добавляем поле телефона site_telephone
    */
    $wp_customize->add_setting(
    // ID
        'site_telephone1',
        // Arguments array
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
    // ID
        'site_telephone_control1',
        // Arguments array
        array(
            'type' => 'option',
            'label' => "Номер телефона в шапке",
            'section' => 'data_header_section',
            // This last one must match setting ID from above
            'settings' => 'site_telephone1'
        )
    );
    $wp_customize->add_section(
    // ID
        'data_footer_section',
        // Arguments array
        array(
            'title' => 'Данные подвала сайта',
            'capability' => 'edit_theme_options',
            'description' => "Тут можно указать данные сайта"
        )
    );

    /*
    Добавляем поле телефона site_telephone
    */
    $wp_customize->add_setting(
    // ID
        'footer_telephone',
        // Arguments array
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
    // ID
        'footer_telephone_control',
        // Arguments array
        array(
            'type' => 'option',
            'label' => "Номер телефона в подвале",
            'section' => 'data_footer_section',
            // This last one must match setting ID from above
            'settings' => 'footer_telephone'
        )
    );
    $wp_customize->add_setting(
    // ID
        'footer_email',
        // Arguments array
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
    // ID
        'footer_email_control',
        // Arguments array
        array(
            'type' => 'option',
            'label' => "Email в подвале",
            'section' => 'data_footer_section',
            // This last one must match setting ID from above
            'settings' => 'footer_email'
        )
    );
    $wp_customize->add_setting(
    // ID
        'footer_address',
        // Arguments array
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
    // ID
        'footer_address_control',
        // Arguments array
        array(
            'type' => 'option',
            'label' => "Адрес в подвале",
            'section' => 'data_footer_section',
            // This last one must match setting ID from above
            'settings' => 'footer_address'
        )
    );

    $wp_customize->add_section(
    // ID
        'social_section',
        // Arguments array
        array(
            'title' => 'Соц сети',
            'capability' => 'edit_theme_options',
            'description' => "Тут можно указать данные сайта"
        )
    );

    /*
    Добавляем поле телефона site_telephone
    */
    $wp_customize->add_setting(
    // ID
        'adtherapy_link',
        // Arguments array
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
    // ID
        'adtherapy_link_control',
        // Arguments array
        array(
            'type' => 'option',
            'label' => "Ссылка Рекламотерапия",
            'section' => 'social_section',
            // This last one must match setting ID from above
            'settings' => 'adtherapy_link'
        )
    );
    $wp_customize->add_setting(
    // ID
        'vk_link',
        // Arguments array
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
    // ID
        'vk_link_control',
        // Arguments array
        array(
            'type' => 'option',
            'label' => "Ссылка Вконтакте",
            'section' => 'social_section',
            // This last one must match setting ID from above
            'settings' => 'vk_link'
        )
    );
    $wp_customize->add_setting(
    // ID
        'instagram_link',
        // Arguments array
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
    // ID
        'instagram_link_control',
        // Arguments array
        array(
            'type' => 'option',
            'label' => "Ссылка Instagramm",
            'section' => 'social_section',
            // This last one must match setting ID from above
            'settings' => 'instagram_link'
        )
    );
    $wp_customize->add_setting(
    // ID
        'watsapp_link',
        // Arguments array
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
    // ID
        'watsapp_link_control',
        // Arguments array
        array(
            'type' => 'option',
            'label' => "Ссылка WatsApp",
            'section' => 'social_section',
            // This last one must match setting ID from above
            'settings' => 'watsapp_link'
        )
    );
    $wp_customize->add_setting(
    // ID
        'viber_link',
        // Arguments array
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
    // ID
        'viber_link_control',
        // Arguments array
        array(
            'type' => 'option',
            'label' => "Ссылка Viber",
            'section' => 'social_section',
            // This last one must match setting ID from above
            'settings' => 'viber_link'
        )
    );
    $wp_customize->add_section(
    // ID
        'moscow',
        // Arguments array
        array(
            'title' => 'Офис в Москве',
            'capability' => 'edit_theme_options',
            'description' => "Тут можно указать данные сайта"
        )
    );

    /*
    Добавляем поле телефона site_telephone
    */
    $wp_customize->add_setting(
    // ID
        'moscow_address',
        // Arguments array
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
    // ID
        'moscow_address_control',
        // Arguments array
        array(
            'type' => 'option',
            'label' => "Адрес",
            'section' => 'moscow',
            // This last one must match setting ID from above
            'settings' => 'moscow_address'
        )
    );
    $wp_customize->add_setting(
    // ID
        'moscow_phone',
        // Arguments array
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
    // ID
        'moscow_phone_control',
        // Arguments array
        array(
            'type' => 'option',
            'label' => "Тел.",
            'section' => 'moscow',
            // This last one must match setting ID from above
            'settings' => 'moscow_phone'
        )
    );
    $wp_customize->add_setting(
    // ID
        'moscow_mobile',
        // Arguments array
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
    // ID
        'moscow_mobile_control',
        // Arguments array
        array(
            'type' => 'option',
            'label' => "Моб.",
            'section' => 'moscow',
            // This last one must match setting ID from above
            'settings' => 'moscow_mobile'
        )
    );
    $wp_customize->add_setting(
    // ID
        'moscow_email',
        // Arguments array
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
    // ID
        'moscow_email_control',
        // Arguments array
        array(
            'type' => 'option',
            'label' => "Email",
            'section' => 'moscow',
            // This last one must match setting ID from above
            'settings' => 'moscow_email'
        )
    );
    $wp_customize->add_setting(
    // ID
        'moscow_site',
        // Arguments array
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
    // ID
        'moscow_site_control',
        // Arguments array
        array(
            'type' => 'option',
            'label' => "Site",
            'section' => 'moscow',
            // This last one must match setting ID from above
            'settings' => 'moscow_site'
        )
    );
    $wp_customize->add_section(
    // ID
        'cyprus',
        // Arguments array
        array(
            'title' => 'Офис на Кипре',
            'capability' => 'edit_theme_options',
            'description' => "Тут можно указать данные сайта"
        )
    );

    /*
    Добавляем поле телефона site_telephone
    */
    $wp_customize->add_setting(
    // ID
        'cyprus_address',
        // Arguments array
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
    // ID
        'cyprus_address_control',
        // Arguments array
        array(
            'type' => 'option',
            'label' => "Адрес",
            'section' => 'cyprus',
            // This last one must match setting ID from above
            'settings' => 'cyprus_address'
        )
    );
    $wp_customize->add_setting(
    // ID
        'cyprus_phone',
        // Arguments array
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
    // ID
        'cyprus_phone_control',
        // Arguments array
        array(
            'type' => 'option',
            'label' => "Тел.",
            'section' => 'cyprus',
            // This last one must match setting ID from above
            'settings' => 'cyprus_phone'
        )
    );
    $wp_customize->add_setting(
    // ID
        'cyprus_mobile',
        // Arguments array
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
    // ID
        'cyprus_mobile_control',
        // Arguments array
        array(
            'type' => 'option',
            'label' => "Моб.",
            'section' => 'cyprus',
            // This last one must match setting ID from above
            'settings' => 'cyprus_mobile'
        )
    );
    $wp_customize->add_setting(
    // ID
        'cyprus_email',
        // Arguments array
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
    // ID
        'cyprus_email_control',
        // Arguments array
        array(
            'type' => 'option',
            'label' => "Email",
            'section' => 'cyprus',
            // This last one must match setting ID from above
            'settings' => 'cyprus_email'
        )
    );
    $wp_customize->add_setting(
    // ID
        'cyprus_site',
        // Arguments array
        array(
            'default' => '',
            'type' => 'option'
        )
    );
    $wp_customize->add_control(
    // ID
        'cyprus_site_control',
        // Arguments array
        array(
            'type' => 'option',
            'label' => "Site",
            'section' => 'cyprus',
            // This last one must match setting ID from above
            'settings' => 'cyprus_site'
        )
    );
}
add_action( 'customize_register', 'mytheme_customize_register' );

?>