<?php
/*
Template Name: О комплексе - Управляющая компания
*/
?>
<?php

get_header();

?>
</div>
</div>
</div>
</div>
<!-- HEADER END-->
<!--  START-->
<div class="row philosophy">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Управляющая компания</h1>
            </div>
            <div class="cpl-md-12 content">
                <?php

                $my_post_obj = get_post(get_the_ID()); // параметр функции - ID поста, содержимое которого нужно вывести
                echo $my_post_obj->post_content;
                ?>
            </div>
        </div>
    </div>
</div>
<!--  END-->

<?php

get_footer();

?>

