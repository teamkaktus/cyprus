<?php
/*
Template Name: Гражданство - Условия получения гражданства
*/
?>
<?php

get_header();

?>
</div>
</div>
</div>
</div>
<!-- HEADER END-->
<!-- CITIZENSHIP STEPS START -->
<div class="row citizenship">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <span class="citizenship-title_steps"><?php the_title(); ?></span>
            </div>
            <?php

            $my_post_obj = get_post(get_the_ID()); // параметр функции - ID поста, содержимое которого нужно вывести
            echo $my_post_obj->post_content;
            ?>
        </div>
    </div>
</div>
<!-- CITIZENSHIP STEPS END -->

<?php

get_footer();

?>
