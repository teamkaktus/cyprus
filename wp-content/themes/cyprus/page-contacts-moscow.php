<?php
/*
Template Name: Контакты - Офис в Москве
*/
?>
<?php

get_header();

?>
</div>
</div>
</div>
</div>
<!-- HEADER END-->
<!-- CONTACT START -->
<div class="row contact">
    <div class="container">
        <div class="row">
            <div class="col-md-12"><span class="contact_header">Офис в Москве</span></div>
        </div>
        <div class="row">
            <div class="col-md-6 col-sm-6">
                <table>
                    <tr>
                        <td>Адрес:</td>
                        <td rowspan="2">
                            <?echo (get_option('moscow_address'))?>
                        </td>
                    </tr>
                    <tr>

                        <td>&nbsp;</td>
                    </tr>
                    <tr>
                        <td>Тел.:</td>
                        <td> <?echo (get_option('moscow_phone'))?></td>
                    </tr>
                    <tr>
                        <td>Моб.:</td>
                        <td> <?echo (get_option('moscow_mobile'))?></td>
                    </tr>
                    <tr>
                        <td>E-mail:</td>
                        <td>
                            <a href="mailto:<?echo (get_option('moscow_email'))?>">
                                <span class="link"> <?echo (get_option('moscow_email'))?></span>
                            </a>
                        </td>
                    </tr>
                    <tr>
                        <td>Сайт:</td>
                        <td>
                            <a href="<?echo (get_option('moscow_site'))?>">
                                <span class="link"> <?echo (get_option('moscow_site'))?></span>
                            </a>
                        </td>
                    </tr>
                </table>
            </div>
            <div class="col-md-6 col-sm-6">

                <div class="row">
                    <div class="col-md-12 callback">Обратная связь</div>
                    <div class="col-md-6 col-sm-6">
                        <input class="name_input" type="text" placeholder="Ваше имя">
                        <input class="phone_input" type="text" placeholder="Телефон/E-mail">
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <textarea class="message" placeholder="Сообщение..."></textarea>
                    </div>
                    <div class="col-md-6 col-sm-6">
                        <input class="send_button" type="button" value="отправить">
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="row map" id="map-moscow">

</div>
<!-- CONTACT END -->
<?php

get_footer();

?>