<?php
/*
Template Name: Генеральный план - Аренда
*/
?>

<?php

get_header();

?>
</div>
</div>
</div>
</div>
<!-- HEADER END-->
<!-- RENT START-->
<div class="row rent">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <span class="masterplan-title">Аренда</span>
            </div>
            <div class="col-md-12">
                <?php

                $my_post_obj = get_post(get_the_ID()); // параметр функции - ID поста, содержимое которого нужно вывести
                echo $my_post_obj->post_content;
                ?>
            </div>
        </div>

    </div>
</div>
<!-- RENT START-->
<!-- RENT-FORM START-->
<div class="row rent-form">
    <div class="container">
        <div class="row">
            <div class="col-md-8">
                <p class="rent-form-t1">Если у Вас есть вопросы по отдыху на вилле Cap St. Gejorges, или Вам нужен
                    совет, что бы определиться
                    с выбором, позвоните нам: </p>
                <p class="rent-form-phone">+7 (495) 984-65-76</p>
                <p class="rent-form-t1">или заполните заявку. </p>
                <p class="rent-form-t2">Мы ответим на все вопросы и подберем лучшие варианты. </p>
                <p class="rent-form-t1">Для Вас это бесплатно. </p>
            </div>
        </div>
        <div class="row">
            <form>
                <div class="col-md-3 col-xs-12">
                    <input type="text" name="name" id="name" placeholder="Ваше имя">
                </div>
                <div class="col-md-3 col-xs-12">
                    <input type="text" name="phone" id="phone" placeholder="Телефон">
                </div>
                <div class="col-md-3 col-xs-12">
                    <input type="button" name="ask" id="ask" value="Задать вопрос">
                </div>
            </form>
        </div>

    </div>
</div>
<!-- RENT-FORM END-->

<?php

get_footer();

?>

