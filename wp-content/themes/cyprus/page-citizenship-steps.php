<?php
/*
Template Name: Гражданство - Этапы и сроки получения
*/
?>

<?
get_header();
?>


            </div>
        </div>
    </div>
</div>
<!-- HEADER END-->

<!-- CONTACT START -->
<div class="row citizenship">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <span class="citizenship-title">Этапы и сроки получения гражданства</span>
            </div>
            <div class="row steps_container ">
                <div class="col-sm-2 text-center">
                    <img class="steps_img" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/citizenship/bank.png" alt="">
                    <div>
                        Открытие в банке Кипра счета клиента/ESCROW не менее 2,5 млн. Евро
                    </div>
                </div>
                <div class="col-sm-1 text-center">
                    <img class="hidden-xs arrows" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/citizenship/arrow1.png" alt="">
                    <img class="visible-xs arrows" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/citizenship/arrow3.png" alt="">

                </div>
                <div class="col-sm-2 text-center">
                    <img class="steps_img" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/citizenship/forma.png" alt="">
                    <div>
                        Подписание договора купли продажи недвижимости (основание для подачи заявления на получение
                        паспорта)
                    </div>
                </div>
                <div class="col-sm-1 text-center">
                    <img class="hidden-xs arrows" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/citizenship/arrow2.png" alt="">
                    <img class="visible-xs arrows" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/citizenship/arrow3.png" alt="">

                </div>
                <div class="col-sm-2 text-center">
                    <img class="steps_img" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/citizenship/pass.png" alt="">
                    <div>
                        Оформление паспорта Кипра всех членов семьи (2-3 мес.)
                    </div>
                </div>
                <div class="col-sm-1 text-center">
                    <img class="hidden-xs arrows" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/citizenship/arrow1.png" alt="">
                    <img class="visible-xs arrows" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/citizenship/arrow3.png" alt="">

                </div>
                <div class="col-sm-2 text-center">
                    <img class="steps_img" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/citizenship/pay.png" alt="">
                    <div>
                        Поэтапная оплата строительства объекта недвижимости (не более 12 мес.)
                    </div>

                </div>
            </div>

        </div>
    </div>
</div>
<!-- CONTACT END -->
<?php
get_footer();
?>
