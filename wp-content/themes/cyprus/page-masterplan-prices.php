<?php
/*
Template Name: Генеральный план - Цены
*/
?>
<?php

get_header();

?>
</div>
</div>
</div>
</div>
<!-- HEADER END-->
<!-- PRICES  START-->
<div class="row masterplan">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <span class="masterplan-title">Цены</span>
            </div>
        </div>
        <div class="row table-title">
            <div class="col-md-12">Продажа Вилл</div>
        </div>
        <?php

        $my_post_obj = get_post(get_the_ID()); // параметр функции - ID поста, содержимое которого нужно вывести
        echo $my_post_obj->post_content;
        ?>
    </div>
</div>
<!-- PRICES  END-->

<?php

get_footer();

?>

