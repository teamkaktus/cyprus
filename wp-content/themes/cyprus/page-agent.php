<?php
/*
Template Name: Agent
*/
?>
<?php
session_start();

if (!isset($_SESSION["is_auth"])) { // Если пользователь авторизован, приветствуем:
    header('Location: /', true, 303);
}
if (isset($_SESSION["login"])) {
    $login = $_SESSION["login"];
} else {
    header('Location: /', true, 303);
}

if (function_exists('wp_enqueue_media')) {
    wp_enqueue_media();
}

define('SHORTINIT', true);
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
global $wpdb;

$query = "SELECT * FROM " . $wpdb->prefix . "cyprus_agent WHERE email = '" . $login . "'";

$agent = $wpdb->get_results($query);


if (isset($_POST['new_client'])) {

    $fio = $_POST["new_fio"];
    $phone = $_POST["new_phone"];
    $email = $_POST["new_email"];
    $reg_date = "" . date('d.m.Y');
    $reg_time = "" . date('H.i');
    $type = $_POST["new_client_type"];
    $status = "отсутствует";
    $conv_date = $_POST["new_date"];
    $conv_time = $_POST["new_time"];

    $query = "INSERT INTO " . $wpdb->prefix . "cyprus_clients
        VALUES ('', '" . $fio . "',
         '" . $phone . "',
         '" . $email . "',
         '" . $reg_date . "',
         '" . $reg_time . "',
         '" . $type . "',
         '" . $status . "',
         '" . $agent[0]->id . "',
         '" . $conv_date . "',
         '" . $conv_time . "'
         )";

    if ($wpdb->query($query)) {
        // echo("Юзверь создан");
        header('Location: /agent', true, 303);
    } else {
        //   echo("Ошибка");
    }


}


get_header();


?>
    </div>
    </div>
    </div>
    </div>
    <!-- HEADER END-->
    <!-- AGENT END-->
    <div class="row agent">
        <div class="container">
            <div class="row">
                <div class="container">
                    <h1>Личный кабинет</h1>
                </div>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <input type="hidden" id="agent_id" value="<?php echo($agent[0]->id);?>">
                    <table class="agent_data">
                        <tr>
                            <td>ФИО</td>
                            <td><?php echo($agent[0]->f . " " . $agent[0]->i . " " . $agent[0]->o . " "); ?></td>
                        </tr>
                        <tr>
                            <td>Телефон</td>
                            <td><?php echo($agent[0]->phone); ?></td>
                        </tr>
                        <tr>
                            <td>E-mail</td>
                            <td><?php echo($agent[0]->email); ?></td>
                        </tr>
                        <tr>
                            <td>Дата регистрации</td>
                            <td><?php echo($agent[0]->regdate); ?></td>
                        </tr>
                        <tr>
                            <td>Вид деятельности</td>
                            <?php
                            $query = "SELECT
wp_cyprus_agent_types.`desc` AS description
FROM
 " . $wpdb->prefix . "cyprus_agent_types
WHERE
wp_cyprus_agent_types.id IN (SELECT type FROM " . $wpdb->prefix . "cyprus_agent WHERE email = '" . $login . "')";


                            $res = $wpdb->get_results($query);
                            ?>
                            <td><?php echo $res[0]->description ?></td>
                        </tr>
                        <tr>
                            <td>Кол-во клиентов</td>
                            <?php
                            $query = "SELECT Count(id) AS c_count
FROM " . $wpdb->prefix . "cyprus_clients AS clients
WHERE clients.agent_id IN (SELECT id FROM " . $wpdb->prefix . "cyprus_agent WHERE email = '" . $login . "')";

                            $res = $wpdb->get_results($query);
                            ?>
                            <td><?php echo $res[0]->c_count ?></td>
                        </tr>
                    </table>
                    <div id="passport_container">
                        <input id="add_new_passport" type="button" value="Обзор...">
                        <input id="new_passport_link" type="text" placeholder="Скан паспорта" disabled>
                    </div>
                    <input id="send_passport" class="buttons" type="button" value="Редактировать">
                </div>
                <div class="col-md-5 col-md-offset-1">
                    <table class="user_data">
                        <tr>
                            <td><span id="user_add_btn" class="text-center"><span></span><p>Добавить клиента</p></span>
                            </td>
                        </tr>
                    </table>

                    <div id="client_form_container">
                        <form id="client_data" method="post" style="display: none">
                            <table class="user_data">
                                <tbody>
                                <tr>
                                    <td><input class="input_data" name="new_fio" type="text" placeholder="ФИО"></td>
                                </tr>
                                <tr>
                                    <td><input class="input_data" name="new_date" type="text" placeholder="Дата беседы"></td>
                                </tr>
                                <tr>
                                    <td><input class="input_data" name="new_time" type="text" placeholder="Время беседы"></td>
                                </tr>
                                <tr>
                                    <td><input class="input_data" name="new_client_type" type="text" placeholder="Тип клиента"></td>
                                </tr>
                                <tr>
                                    <td><input class="input_data" name="new_phone" type="text" placeholder="Телефон"></td>
                                </tr>
                                <tr>
                                    <td><input class="input_data" name="new_email" type="text" placeholder="E-mail"></td>
                                </tr>
                                <tr>
                                    <td><input class="buttons" id="add_client" type="submit" name="new_client" value="Добавить"></td>
                                </tr>
                                </tbody>
                            </table>
                        </form>
                    </div>

                </div>
            </div>
            <div class="row">
                <div class="container">
                    <div class="col-md-12">
                        <?php
                        $agent;
                        $query = "SELECT *
FROM " . $wpdb->prefix . "cyprus_clients AS clients
WHERE clients.agent_id IN (SELECT id FROM wp_cyprus_agent WHERE email = '" . $login . "')";

                        $res = $wpdb->get_results($query);
                        if (isset($res[0])) {
                            ?>
                            <table class="clients">
                                <tr>
                                    <th>№п.п.</th>
                                    <th>ФИО клиента</th>
                                    <th>Телефон</th>
                                    <th>E-mail</th>
                                    <th>Дата регистр.</th>
                                    <th>Время регистр.</th>
                                    <th>Тип клиента</th>
                                    <th>Статус клиента</th>
                                    <th>ФИО агента</th>
                                    <th>Телефон агента</th>
                                    <th>Дата беседы</th>
                                    <th>Время беседы</th>
                                </tr>
                                <?php

                                $i = 1;
                                foreach ($res as $client) {


                                    ?>
                                    <tr>
                                        <td><?php echo $i++ ?></td>
                                        <td><?php echo $client->fio ?></td>
                                        <td><?php echo $client->phone ?></td>
                                        <td><?php echo $client->email ?></td>
                                        <td><?php echo $client->reg_date ?></td>
                                        <td><?php echo $client->reg_time ?></td>
                                        <td><?php echo $client->type ?></td>
                                        <td><?php echo $client->status ?></td>
                                        <td><?php echo($agent[0]->f . " " . $agent[0]->i . " " . $agent[0]->o . " "); ?></td>
                                        <td><?php echo($agent[0]->phone); ?></td>
                                        <td><?php echo $client->conv_date ?></td>
                                        <td><?php echo $client->conv_time ?></td>
                                    </tr>
                                    <?php
                                }
                                ?>

                            </table>
                            <?php
                        }
                        ?>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- AGENT END-->

<?php

get_footer();

?>