<?php
/*
Template Name: Гражданство - Почему Кипр
*/
?>
<?php 

get_header();

?>
</div>
</div>
</div>
</div>
<!-- HEADER END-->
<!-- CITIZENSHIP STEPS START -->
<div class="row citizenship">
    <div class="container">
        <div class="row">
            <div class="col-sm-12">
                <span class="citizenship-title_steps"><?php the_title(); ?></span>
            </div>
            <div class="col-sm-12 list">
                <?php

                $my_post_obj = get_post(get_the_ID()); // параметр функции - ID поста, содержимое которого нужно вывести
                echo $my_post_obj->post_content;
                ?>


            </div>
        </div>
    </div>
</div>
<!-- CITIZENSHIP STEPS END -->
<?php

get_footer();

?>