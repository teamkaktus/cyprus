<?php
/*
Template Name: Инфраструктура - Внутренняя
*/
?>
<?php

get_header();

?>
    </div>
    </div>
    </div>
    </div>
    <!-- HEADER END-->

    <!-- INFRASTRUCTURE START -->
    <div class="row infrastructure">

        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <span class="infrastructure-title">Внутренняя инфраструктура</span>
                </div>

            </div>
            <div class="row inf">
                <div class="inf-tabs col-sm-4 col-xs-12">

                    <!-- Nav tabs -->
                    <ul class="nav nav-tabs nav-stacked" role="tablist">
                        <li role="presentation" <?php
                        if($_GET["p"]=="reception" || !isset($_GET["p"]) || $_GET["p"]==""){
                            echo("class=\"active\"");
                        }
                        ?>>
                            <a href="#reception" aria-controls="reception" role="tab" data-toggle="tab"><span></span>24/7
                                ресепшн</a>
                        </li>
                        <li role="presentation" <?php
                        if($_GET["p"]=="security"){
                            echo("class=\"active\"");
                        }
                        ?>>
                            <a href="#security" aria-controls="security" role="tab" data-toggle="tab"><span></span>Охрана</a>
                        </li>
                        <li role="presentation" <?php
                        if($_GET["p"]=="clubhouse"){
                            echo("class=\"active\"");
                        }
                        ?>>
                            <a href="#clubhouse" aria-controls="clubhouse" role="tab" data-toggle="tab"><span></span>Клабхаус</a>
                        </li>
                        <li role="presentation" <?php
                        if($_GET["p"]=="spa"){
                            echo("class=\"active\"");
                        }
                        ?>>
                            <a href="#spa" aria-controls="spa" role="tab" data-toggle="tab"><span></span>СПА</a>
                        </li>
                        <li role="presentation" <?php
                        if($_GET["p"]=="sport"){
                            echo("class=\"active\"");
                        }
                        ?>>
                            <a href="#sport" aria-controls="sport" role="tab" data-toggle="tab"><span></span>Спортивная
                                зона</a>
                        </li>
                        <li role="presentation" <?php
                        if($_GET["p"]=="auditorium"){
                            echo("class=\"active\"");
                        }
                        ?>>
                            <a href="#auditorium" aria-controls="auditorium" role="tab" data-toggle="tab"><span></span>Конференц
                                зал</a>
                        </li>
                        <li role="presentation" <?php
                        if($_GET["p"]=="cinemahall"){
                            echo("class=\"active\"");
                        }
                        ?>>
                            <a href="#cinemahall" aria-controls="cinemahall" role="tab" data-toggle="tab"><span></span>Кинотеатр</a>
                        </li>
                        <li role="presentation" <?php
                        if($_GET["p"]=="caffe"){
                            echo("class=\"active\"");
                        }
                        ?>>
                            <a href="#caffe" aria-controls="caffe" role="tab"
                               data-toggle="tab"><span></span>Ресторан</a>
                        </li>
                        <li role="presentation" <?php
                        if($_GET["p"]=="helipad"){
                            echo("class=\"active\"");
                        }
                        ?>>
                            <a href="#helipad" aria-controls="helipad" role="tab" data-toggle="tab"><span></span>Вертолётная
                                площадка</a>
                        </li>
                    </ul>
                </div>
                <div class="inf-content col-sm-8 col-xs-12">
                    <!-- Tab panes -->
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane <?php
                        if($_GET["p"]=="reception" || !isset($_GET["p"]) || $_GET["p"]==""){
                            echo("active");
                        }
                        ?>" id="reception">

                            <?php

                            $my_post_obj = get_post(187); // параметр функции - ID поста, содержимое которого нужно вывести
                            echo $my_post_obj->post_content;
                            ?>

                        </div>
                        <div role="tabpanel" class="tab-pane <?php
                        if($_GET["p"]=="security"){
                            echo("active");
                        }
                        ?>" id="security">

                            <?php

                            $my_post_obj = get_post(192); // параметр функции - ID поста, содержимое которого нужно вывести
                            echo $my_post_obj->post_content;
                            ?>

                        </div>
                        <div role="tabpanel" class="tab-pane <?php
                        if($_GET["p"]=="clubhouse"){
                            echo("active");
                        }
                        ?>" id="clubhouse">

                            <?php

                            $my_post_obj = get_post(195); // параметр функции - ID поста, содержимое которого нужно вывести
                            echo $my_post_obj->post_content;
                            ?>

                        </div>
                        <div role="tabpanel" class="tab-pane <?php
                        if($_GET["p"]=="spa"){
                            echo("active");
                        }
                        ?>" id="spa">

                            <?php

                            $my_post_obj = get_post(197); // параметр функции - ID поста, содержимое которого нужно вывести
                            echo $my_post_obj->post_content;
                            ?>

                        </div>
                        <div role="tabpanel" class="tab-pane <?php
                        if($_GET["p"]=="sport"){
                            echo("active");
                        }
                        ?>" id="sport">

                            <?php

                            $my_post_obj = get_post(200); // параметр функции - ID поста, содержимое которого нужно вывести
                            echo $my_post_obj->post_content;
                            ?>

                        </div>
                        <div role="tabpanel" class="tab-pane <?php
                        if($_GET["p"]=="auditorium"){
                            echo("active");
                        }
                        ?>" id="auditorium">

                            <?php

                            $my_post_obj = get_post(202); // параметр функции - ID поста, содержимое которого нужно вывести
                            echo $my_post_obj->post_content;
                            ?>

                        </div>
                        <div role="tabpanel" class="tab-pane <?php
                        if($_GET["p"]=="cinemahall"){
                            echo("active");
                        }
                        ?>" id="cinemahall">

                            <?php

                            $my_post_obj = get_post(204); // параметр функции - ID поста, содержимое которого нужно вывести
                            echo $my_post_obj->post_content;
                            ?>

                        </div>
                        <div role="tabpanel" class="tab-pane <?php
                        if($_GET["p"]=="caffe"){
                            echo("active");
                        }
                        ?>" id="caffe">

                            <?php

                            $my_post_obj = get_post(206); // параметр функции - ID поста, содержимое которого нужно вывести
                            echo $my_post_obj->post_content;
                            ?>

                        </div>
                        <div role="tabpanel" class="tab-pane <?php
                        if($_GET["p"]=="helipad"){
                            echo("active");
                        }
                        ?>" id="helipad">

                            <?php

                            $my_post_obj = get_post(209); // параметр функции - ID поста, содержимое которого нужно вывести
                            echo $my_post_obj->post_content;
                            ?>

                        </div>
                    </div>

                </div>
            </div>

        </div>

    </div>
    <!-- INFRASTRUCTURE END -->
<?php

get_footer();

?>