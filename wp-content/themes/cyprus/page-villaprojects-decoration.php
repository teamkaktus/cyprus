<?php
/*
Template Name: Проекты вилл - Отделка
*/
?>
<?php

get_header();

?>
</div>
</div>
</div>
</div>
<!-- HEADER END-->
<!-- DECORATION START-->
<div class="row decoration">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Отделка</h1>
            </div>
            <div class="col-md-12 content">
                <?php

                $my_post_obj = get_post(get_the_ID()); // параметр функции - ID поста, содержимое которого нужно вывести
                echo $my_post_obj->post_content;
                ?>
            </div>
        </div>


    </div>
</div>
<!-- DECORATION END-->

<?php

get_footer();

?>
