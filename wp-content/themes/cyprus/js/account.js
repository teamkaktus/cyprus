/**
 * Created by Torvald on 14.05.2016.
 */

$(document).ready(function(){
    $("#user_add_btn").click(function(){
        $("#client_data").css("display","inherit");
    });

    $('#add_new_passport').click(function(e) {
        var custom_logo_uploader;
        e.preventDefault();
        //If the uploader object has already been created, reopen the dialog
        if (custom_logo_uploader) {
            custom_logo_uploader.open();
            return;
        }
        //Extend the wp.media object
        custom_logo_uploader = wp.media.frames.file_frame = wp.media({
            title: 'Выберите изображения',
            button: {
                text: 'Выбрать'
            },
            multiple: false
        });
        //When a file is selected, grab the URL and set it as the text field's value
        custom_logo_uploader.on('select', function() {
            attachment = custom_logo_uploader.state().get('selection').first().toJSON();

            $("#new_passport_link").val(attachment.url);

        });
        //Open the uploader dialog
        custom_logo_uploader.open();
    });

    $("#send_passport").click(function(){

        var agent = new Object();
        agent.id = $("#agent_id").val();
        agent.new_passport = $("#new_passport_link").val();

        var res = JSON.stringify(agent);

        $.ajax({
                url: acc_ajax.url,
                type: 'POST',
                data: {
                    event : 'add_passport',
                    data  : res
                },
            })
            .done(function(data) {
                $("#new_passport_link").val("");
                alert(data);
            });
    });
});

