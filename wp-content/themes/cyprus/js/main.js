/**
 * Created by Torvald on 31.03.2016.
 */
$(document).ready(function () {

    window.lfw = 870;
    window.lfh = 500;
    window.sfw = 600;
    window.sfh = 400;

    /*if (window.screen.width >= 1200) {
     window.lfw = 870;
     window.lfh = 500;
     window.sfw = 600;
     window.sfh = 400;
     } else if (window.screen.width >= 992) {
     window.lfw = 719;
     window.lfh = 413;
     window.sfw = 496;
     window.sfh = 330;
     } else if (window.screen.width >= 768) {
     window.lfw = 556;
     window.lfh = 320;
     window.sfw = 384;
     window.sfh = 256;
     } else {
     window.lfw = 464;
     window.lfh = 266;
     window.sfw = 320;
     window.sfh = 213;
     }*/
    window.carousel = $("#carousel").featureCarousel({
        autoPlay: 0,
        largeFeatureWidth: window.lfw,
        largeFeatureHeight: window.lfh,
        smallFeatureWidth: window.sfw,
        smallFeatureHeight: window.sfh,
        sidePadding: 0,
        topPadding: 0
    });

    $("#but_prev").click(function () {
        carousel.prev();
    });
    $("#but_pause").click(function () {
        carousel.pause();
    });
    $("#but_start").click(function () {
        carousel.start();
    });
    $("#but_next").click(function () {
        carousel.next();
    });

    /*$(window).resize(function () {
     if (window.screen.width >= 1200) {
     window.lfw = 870;
     window.lfh = 500;
     window.sfw = 600;
     window.sfh = 400;
     } else if (window.screen.width >= 992) {
     window.lfw = 719;
     window.lfh = 413;
     window.sfw = 496;
     window.sfh = 330;
     } else if (window.screen.width >= 768) {
     window.lfw = 556;
     window.lfh = 320;
     window.sfw = 384;
     window.sfh = 256;
     } else {
     window.lfw = 464;
     window.lfh = 266;
     window.sfw = 320;
     window.sfh = 213;
     }
     window.carousel = $("#carousel").featureCarousel({
     autoPlay: 0,
     largeFeatureWidth: window.lfw,
     largeFeatureHeight: window.lfh,
     smallFeatureWidth: window.sfw,
     smallFeatureHeight: window.sfh,
     sidePadding: 0,
     topPadding: 0
     });

     $("#but_prev").click(function () {
     carousel.prev();
     });
     $("#but_pause").click(function () {
     carousel.pause();
     });
     $("#but_start").click(function () {
     carousel.start();
     });
     $("#but_next").click(function () {
     carousel.next();
     });
     });*/

    $("#show-all-img-btn").click(function(){
        //$("#img-container").attr();
        $("#img-container").animate({height: "900"}, 1000);
        $("#img-hider").animate({height: "100"}, 1000);
        $("#show-all-img-btn").animate({opacity: "hide", height: "hide"}, 1000);

    });
});