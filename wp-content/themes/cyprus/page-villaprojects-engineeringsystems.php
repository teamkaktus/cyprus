<?php
/*
Template Name: Проекты вилл - Инженерные системы
*/
?>
<?php

get_header();

?>
</div>
</div>
</div>
</div>
<!-- HEADER END-->
<!-- ENGENEERINGSYSTEMS START-->
<div class="row construction">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Инженерные системы</h1>
            </div>
            <div class="col-md-12 content">
                <?php

                $my_post_obj = get_post(get_the_ID()); // параметр функции - ID поста, содержимое которого нужно вывести
                echo $my_post_obj->post_content;
                ?>

            </div>

        </div>
    </div>
</div>
<!-- ENGENEERINGSYSTEMS END-->

<?php

get_footer();

?>
