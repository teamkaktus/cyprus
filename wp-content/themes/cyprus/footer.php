<!-- FOOTER START -->
<div class="footer">
    <div class="container footer-container text-center">
        <div class="row">
            <div class="col-md-1 col-xs-12 footer-big-logo">
                <img class="" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-footer.png" alt="">
            </div>
            <div class="footer-logo col-md-4 col-xs-12 ">
                <img class="" src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/logo-text-footer.png" alt="">
                <p class="hidden-xs copyright">С 2016 | Cap St Georges. Все права защищены</p>
            </div>
            <div class="col-md-3 col-xs-12">
                <span class="footer-phone"><?echo (get_option('footer_telephone'))?></span><br>
                <span class="footer-smalltext">E-mail: <?echo (get_option('footer_email'))?></span><br>
                <span class="footer-smalltext">Адрес: <?echo (get_option('footer_address'))?></span>
            </div>
            <div class="col-md-3 col-md-offset-1 col-xs-12 footer-social">
                <div class="hidden-sm hidden-xs">
                    <a href="http://<?php echo (get_option('vk_link'))?>">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/social_vk.png"" alt="">
                    </a>
                    <a href="http://<?php echo (get_option('instagram_link'))?>">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/social_instagram.png"" alt="">
                    </a>
                    <a href="http://<?php echo (get_option('watsapp_link'))?>">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/social_watsapp.png"" alt="">
                    </a>
                    <a href="http://<?php echo (get_option('viber_link'))?>">
                        <img src="<?php echo esc_url( get_template_directory_uri() ); ?>/images/social_viber.png"" alt="">
                    </a>
                </div>
                <div class="">Разработано в <a href="<?php echo (get_option('adtherapy_link'))?>">Рекламотерапия</a></div>
            </div>
            <div>
                <p class="visible-xs copyright">С 2016 | Cap St Georges. Все права защищены</p>
            </div>
        </div>

    </div>
</div>
<!-- FOOTER END -->
<?php

wp_footer();

?>
</body>
</html>
