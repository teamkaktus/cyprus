<?php
/*
Template Name: О комплексе - Философия
*/
?>
<?php

get_header();

?>
</div>
</div>
</div>
</div>
<!-- HEADER END-->
<!-- PHILOSOPHY START-->
<div class="row philosophy">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Философия</h1>
            </div>
            <div class="cpl-md-12 content">
                <?php

                $my_post_obj = get_post(get_the_ID()); // параметр функции - ID поста, содержимое которого нужно вывести
                echo $my_post_obj->post_content;
                ?>
            </div>
        </div>
    </div>
</div>
<!-- PHILOSOPHY END-->
<?php

get_footer();

?>

