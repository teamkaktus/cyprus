<?php
/*
Template Name: О комплексе - Клубный дом
*/
?>
<?php

get_header();

?>
</div>
</div>
</div>
</div>
<!-- HEADER END-->
<!-- CLUBHOUSE START-->
<div class="row philosophy">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Клубный дом</h1>
            </div>
            <div class="cpl-md-12 content">
                <?php

                $my_post_obj = get_post(get_the_ID()); // параметр функции - ID поста, содержимое которого нужно вывести
                echo $my_post_obj->post_content;
                ?>
            </div>
        </div>
    </div>
</div>
<!-- CLUBHOUSE END-->

<?php

get_footer();

?>

