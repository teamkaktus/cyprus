<?php
/*
Template Name: Галерея - Фото
*/
?>

<?php

get_header();

?>
</div>
</div>
</div>
</div>
<!-- HEADER END-->
<!-- START-->
<div class="row">
    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <h1>Фото</h1>
            </div>
        </div>
    </div>
</div>
<div class="row gallery">
    <div class="inf-tabs col-md-2 col-sm-2 col-xs-12">

        <!-- Nav tabs -->
        <ul class="nav nav-tabs nav-stacked" role="tablist">
            <li role="presentation" class="active">
                <a href="#scenery" aria-controls="scenery" role="tab" data-toggle="tab">Пейзажи</a>
            </li>
            <li role="presentation">
                <a href="#inf" aria-controls="inf" role="tab" data-toggle="tab">Инфраструктура</a>
            </li>
            <li role="presentation">
                <a href="#villas" aria-controls="villas" role="tab" data-toggle="tab">Вилы</a>
            </li>
            <li role="presentation">
                <a href="#winter" aria-controls="winter" role="tab" data-toggle="tab">Зима</a>
            </li>
            <li role="presentation">
                <a href="#autumn" aria-controls="autumn" role="tab" data-toggle="tab">Осень</a>
            </li>
            <li role="presentation">
                <a href="#sight" aria-controls="sight" role="tab" data-toggle="tab">Достопримечательности</a>
            </li>
            <li role="presentation">
                <a href="#sport" aria-controls="sport" role="tab" data-toggle="tab">Спорт</a>
            </li>
            <li role="presentation">
                <a href="#build_steps" aria-controls="build_steps" role="tab" data-toggle="tab">Этапы строительства</a>
            </li>
            <li role="presentation">
                <a href="#landscape" aria-controls="landscape" role="tab" data-toggle="tab">Ландшафты</a>
            </li>
        </ul>
    </div>
    <div class="inf-content col-md-10 col-sm-10 col-xs-12">
        <!-- Tab panes -->
        <div class="tab-content">
            <div role="tabpanel" class="tab-pane active" id="scenery" >
                <?php echo photo_gallery(3); ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="inf">
                <?php echo photo_gallery(7); ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="villas">
                <?php echo photo_gallery(8); ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="winter">
                <?php echo photo_gallery(9); ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="autumn">
                <?php echo photo_gallery(10); ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="sight">
                <?php echo photo_gallery(6); ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="sport">
                <?php echo photo_gallery(11); ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="build_steps">
                <?php echo photo_gallery(12); ?>
            </div>
            <div role="tabpanel" class="tab-pane" id="landscape">
                <?php echo photo_gallery(13); ?>
            </div>

        </div>

    </div>
</div>
<!-- END-->

<?php

get_footer();

?>

