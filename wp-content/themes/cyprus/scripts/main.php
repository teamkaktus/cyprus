<?php
/**
 * Created by PhpStorm.
 * User: Torvald
 * Date: 25.05.2016
 * Time: 22:05
 */
define('SHORTINIT', true);
require_once($_SERVER['DOCUMENT_ROOT'] . '/wp-load.php');
global $wpdb;

function get_all_news($db)
{
    $query = "SELECT * FROM " . $db->prefix . "cyprus_news AS news ORDER BY news.`date` DESC LIMIT 4";
    $items = $db->get_results($query);
    if (isset($items[0])) {
        return $items;
    }
    return NULL;
}