<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title><?php the_title(); ?></title>


    <?php

    wp_head();

    ?>
    <?php
    //var_dump(get_the_ID());
    $page_ID = get_the_ID();
    if ($page_ID == 31 || $page_ID == 78 || $page_ID == 80) {
        ?>
        <script async defer
                src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDxp7OUsDkNBe3xpQ9SNVN7_pOlFKl4LBI&callback=initMap">
        </script>
        <?php
    }
    ?>
</head>
<body>

<!-- HEADER START-->
<div class="row header">
    <div class="container">
        <div class="col-md-12">
            <div class="row hidden-xs">
                <div class="col-md-2 col-sm-2">
                    <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>">
                        <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/logo.png">
                    </a>
                </div>
                <div class="col-md-6 col-md-offset-4 col-sm-6 col-sm-offset-4">
                    <div class="row ">
                        <div class="col-md-2 col-md-offset-3 col-sm-2 view360 text-right">
                            <div class="text-center">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/view360.png"
                                     alt="">
                                <p class="smalltext-header">
                                    <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/gallery/panorama">Обзор 360</a>
                                </p>
                            </div>
                        </div>
                        <div class="col-md-2 col-sm-2 enter-button text-right">
                            <div class="text-center">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/key.png" alt="">
                                <p class="smalltext-header">
                                    <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/regenter/">Вход</a>
                                </p>
                            </div>

                        </div>
                        <div class="col-md-5 col-sm-7 text-right">
                            <span class="phone-header"><?echo (get_option('site_telephone1'))?></span>
                            <button class="callback-header">Обратный звонок</button>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-12 menu">

            <nav class="navbar" role="navigation">
                <div class="container-fluid">
                    <!-- Название компании и кнопка, которая отображается для мобильных устройств группируются для лучшего отображения при свертывание -->
                    <div class="navbar-header">
                        <button type="button" class="navbar-toggle pull-left menu-button-xs" data-toggle="collapse"
                                data-target="#bs-example-navbar-collapse-1">
                            <span class=""></span>
                        </button>
                        <div class="visible-xs text-center">
                            <div class="menu-logo-text">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/menu-logo.png"
                                     alt="">
                            </div>

                            <div class="menu-enter-button">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/menu-key.png"
                                     alt="">
                                <span class="">
                                    <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/regenter/">Вход</a>
                                </span>
                            </div>
                            <div class="menu-view360">
                                <img src="<?php echo esc_url(get_template_directory_uri()); ?>/images/menu-view.png"
                                     alt="">
                                <span class="">
                                    <a href="<?php echo('http://' . $_SERVER["SERVER_NAME"]) ?>/gallery/panorama">Обзор 360</a>
                                </span>
                            </div>
                        </div>
                    </div>

                    <!-- Группируем ссылки, формы, выпадающее меню и прочие элементы -->
                    <div class="collapse navbar-collapse" id="bs-example-navbar-collapse-1">
                        <?php
                        //custom_menu('Главное меню');
                        my_nav_menu(array(
                                'menu_class' => 'nav navbar-nav',
                                'walker' => new My_Walker_Nav_Menu
                            )
                        );

                        ?>
                    </div><!-- /.navbar-collapse -->
                </div><!-- /.container-fluid -->
            </nav>


        </div>
        <div class="col-md-12">
            <div class="row">
                <div class="col-xs-12 visible-xs text-center">
                    <span class="phone-header-xs"><?echo (get_option('site_telephone1'))?></span>
                    <button class="callback-header-xs">Обратный звонок</button>
                </div>
